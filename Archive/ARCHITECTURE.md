- Client/
  - Assets/
  - Exceptions/
  - Migrations/
  - Models/
  - Services/
  - Styles/
  - ViewModels/
  - Views/
- developer-scripts/
- packages/
- Server/
- Tests/
  - Client.UI/
  - Client.Unit/
  - Client.Backend.Integration/
  - Common/
  - EndToEnd/
  - Server.Integration/


**Note:** the plan is to decompose the Client project into 3 separate
projects:
- Clients/Client.Desktop: contains desktop-client view-layer
- Clients/Client.Mobile: contains mobile-client view-layer
- Clients/Client.Shared: contains client view-model and model layers as well as
shared view-layer data
