// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.ViewModels;
using TaskVaultNative.Tests.Client.Backend.Integration.Common;

namespace TaskVaultNative.Tests.Client.Backend.Integration.Flows;

[TestFixture]
[NonParallelizable]
public class CreateLocalVault : ViewModelIntegrationTest
{
    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }

    [Test]
    public void ReturnToLocalVaultSelectFromNewLocalVaultForm()
    {
        var localVaultSelectViewModel = (LocalVaultSelectViewModel)CurrentViewModel;
        localVaultSelectViewModel.NavigateToNewLocalVaultFormView.Execute();
        var newLocalVaultFormViewModel = (NewLocalVaultFormViewModel)CurrentViewModel;

        newLocalVaultFormViewModel.NavigateBack.Execute().Subscribe();

        MainWindowViewModel.CurrentViewUrl().Should().Be("local-vault-select");
    }

    [Test]
    public void ReturnToLocalVaultSelectFromEncryptionAtRestForm()
    {
        var localVaultSelectViewModel = (LocalVaultSelectViewModel)CurrentViewModel;
        localVaultSelectViewModel.NavigateToNewLocalVaultFormView.Execute();
        var newLocalVaultFormViewModel = (NewLocalVaultFormViewModel)CurrentViewModel;
        newLocalVaultFormViewModel.LocalVaultName = "valid vault name";
        newLocalVaultFormViewModel.SubmitForm.Execute().Subscribe();
        var encryptionAtRestFormViewModel = (EncryptionAtRestFormViewModel)CurrentViewModel;

        encryptionAtRestFormViewModel.CancelProcess.Execute().Subscribe();

        MainWindowViewModel.CurrentViewUrl().Should().Be("local-vault-select");
    }

    [Test]
    public void ReturnToNewLocalVaultFormFromEncryptionAtRestForm()
    {
        var localVaultSelectViewModel = (LocalVaultSelectViewModel)CurrentViewModel;
        localVaultSelectViewModel.NavigateToNewLocalVaultFormView.Execute();
        var newLocalVaultFormViewModel = (NewLocalVaultFormViewModel)CurrentViewModel;
        newLocalVaultFormViewModel.LocalVaultName = "valid vault name";
        newLocalVaultFormViewModel.SubmitForm.Execute().Subscribe();
        var encryptionAtRestFormViewModel = (EncryptionAtRestFormViewModel)CurrentViewModel;

        encryptionAtRestFormViewModel.NavigateBack.Execute().Subscribe();

        MainWindowViewModel.CurrentViewUrl().Should().Be("new-local-vault-form");
    }

    [Test]
    public void ReturnToEncryptionAtRestFormFromConnectToRemoteForm()
    {
        var localVaultSelectViewModel = (LocalVaultSelectViewModel)CurrentViewModel;
        localVaultSelectViewModel.NavigateToNewLocalVaultFormView.Execute();
        var newLocalVaultFormViewModel = (NewLocalVaultFormViewModel)CurrentViewModel;
        newLocalVaultFormViewModel.LocalVaultName = "valid vault name";
        newLocalVaultFormViewModel.SubmitForm.Execute().Subscribe();
        var encryptionAtRestFormViewModel = (EncryptionAtRestFormViewModel)CurrentViewModel;
        encryptionAtRestFormViewModel.DeclineToConfigure.Execute().Subscribe();
        var connectToRemoteFormViewModel = (ConnectToRemoteFormViewModel)CurrentViewModel;

        connectToRemoteFormViewModel.NavigateBack.Execute().Subscribe();

        MainWindowViewModel.CurrentViewUrl().Should().Be("encryption-at-rest-form");
    }

    [Test]
    public void ReturnToLocalVaultSelectFromConnectToRemoteForm()
    {
        var localVaultSelectViewModel = (LocalVaultSelectViewModel)CurrentViewModel;
        localVaultSelectViewModel.NavigateToNewLocalVaultFormView.Execute();
        var newLocalVaultFormViewModel = (NewLocalVaultFormViewModel)CurrentViewModel;
        newLocalVaultFormViewModel.LocalVaultName = "valid vault name";
        newLocalVaultFormViewModel.SubmitForm.Execute().Subscribe();
        var encryptionAtRestFormViewModel = (EncryptionAtRestFormViewModel)CurrentViewModel;
        encryptionAtRestFormViewModel.DeclineToConfigure.Execute().Subscribe();
        var connectToRemoteFormViewModel = (ConnectToRemoteFormViewModel)CurrentViewModel;

        connectToRemoteFormViewModel.CancelProcess.Execute().Subscribe();

        MainWindowViewModel.CurrentViewUrl().Should().Be("local-vault-select");
    }
}