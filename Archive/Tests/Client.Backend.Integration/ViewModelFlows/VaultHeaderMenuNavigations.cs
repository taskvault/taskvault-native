// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.ViewModels;
using TaskVaultNative.Tests.Client.Backend.Integration.Common;

namespace TaskVaultNative.Tests.Client.Backend.Integration.Flows;

[TestFixture]
public class VaultHeaderMenuNavigations : ViewModelIntegrationTest
{
    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }

    [Test]
    public void NavigateFromArbitraryViewModelInVaultToMainMenuUsingNavigationMenu()
    {
        _dataGenerator.AddTestLocalVault();
        MainWindowViewModel.ResetAppForUiTesting.Execute().Subscribe();

        var localVaultSelectViewModel = (LocalVaultSelectViewModel)CurrentViewModel;
        localVaultSelectViewModel.LocalVaultsPaginatedList.SelectedItem =
            localVaultSelectViewModel.LocalVaultsPaginatedList.CurrentPageContent.First();
        localVaultSelectViewModel.OpenSelectedVault.Execute().Subscribe();

        var mainMenuViewModel = (MainMenuViewModel)CurrentViewModel;
        mainMenuViewModel.NavigateToProjectGroupsView.Execute().Subscribe();

        var projectGroupsViewModel = (ProjectGroupsViewModel)CurrentViewModel;
        projectGroupsViewModel.HeaderMenu.NavigateToMainMenu.Execute().Subscribe();

        CurrentViewModel.Should().BeOfType<MainMenuViewModel>();
    }

    [Test]
    public void NavigateFromMainMenuToInboxUsingNavigationMenu()
    {
        _dataGenerator.AddTestLocalVault();
        MainWindowViewModel.ResetAppForUiTesting.Execute().Subscribe();

        var localVaultSelectViewModel = (LocalVaultSelectViewModel)CurrentViewModel;
        localVaultSelectViewModel.LocalVaultsPaginatedList.SelectedItem =
            localVaultSelectViewModel.LocalVaultsPaginatedList.CurrentPageContent.First();
        localVaultSelectViewModel.OpenSelectedVault.Execute().Subscribe();

        var mainMenuViewModel = (MainMenuViewModel)CurrentViewModel;

        mainMenuViewModel.HeaderMenu.NavigateToInbox.Execute().Subscribe();

        CurrentViewModel.Should().BeOfType<InboxViewModel>();
    }
}