// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.ViewModels;
using TaskVaultNative.Tests.Client.Backend.Integration.Common;

namespace TaskVaultNative.Tests.Client.Backend.Integration.Flows;

[TestFixture]
public class ReverseNavigations : ViewModelIntegrationTest
{
    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }

    [Test]
    public void NavigateFromProjectGroupsViewModelBackToMainMenuViewModel()
    {
        _dataGenerator.AddTestLocalVault();
        MainWindowViewModel.ResetAppForUiTesting.Execute().Subscribe();

        var localVaultSelectViewModel = (LocalVaultSelectViewModel)CurrentViewModel;
        localVaultSelectViewModel.LocalVaultsPaginatedList.SelectedItem =
            localVaultSelectViewModel.LocalVaultsPaginatedList.CurrentPageContent.First();
        localVaultSelectViewModel.OpenSelectedVault.Execute().Subscribe();

        var mainMenuViewModel = (MainMenuViewModel)CurrentViewModel;
        mainMenuViewModel.NavigateToProjectGroupsView.Execute().Subscribe();

        var projectGroupsViewModel = (ProjectGroupsViewModel)CurrentViewModel;
        projectGroupsViewModel.NavigateBack.Execute().Subscribe();

        CurrentViewModel.Should().BeOfType<MainMenuViewModel>();
    }

    [Test]
    public void CloseNewProjectGroupForm()
    {
        _dataGenerator.AddTestLocalVault();
        MainWindowViewModel.ResetAppForUiTesting.Execute().Subscribe();

        var localVaultSelectViewModel = (LocalVaultSelectViewModel)CurrentViewModel;
        localVaultSelectViewModel.LocalVaultsPaginatedList.SelectedItem =
            localVaultSelectViewModel.LocalVaultsPaginatedList.CurrentPageContent.First();
        localVaultSelectViewModel.OpenSelectedVault.Execute().Subscribe();

        var mainMenuViewModel = (MainMenuViewModel)CurrentViewModel;
        mainMenuViewModel.NavigateToProjectGroupsView.Execute().Subscribe();

        var projectGroupsViewModel = (ProjectGroupsViewModel)CurrentViewModel;
        projectGroupsViewModel.OpenNewProjectGroupForm.Execute().Subscribe();
        var newProjectGroupFormViewModel =
            (NewProjectGroupFormViewModel)projectGroupsViewModel.DetailsScreen.CurrentViewModel;
        newProjectGroupFormViewModel.CloseForm.Execute().Subscribe();

        projectGroupsViewModel.DetailsScreen.CurrentViewModel.Should().BeNull();
    }

    [Test]
    public void NavigateBackFromProjectsViewModel()
    {
        _dataGenerator.AddTestLocalVault();
        _dataGenerator.AddProjectGroupForLocalVault("Test Local Vault");
        MainWindowViewModel.ResetAppForUiTesting.Execute().Subscribe();

        var localVaultSelectViewModel = (LocalVaultSelectViewModel)CurrentViewModel;
        localVaultSelectViewModel.LocalVaultsPaginatedList.SelectedItem =
            localVaultSelectViewModel.LocalVaultsPaginatedList.CurrentPageContent.First();
        localVaultSelectViewModel.OpenSelectedVault.Execute().Subscribe();

        var mainMenuViewModel = (MainMenuViewModel)CurrentViewModel;
        mainMenuViewModel.NavigateToProjectGroupsView.Execute().Subscribe();

        var projectGroupsViewModel = (ProjectGroupsViewModel)CurrentViewModel;
        projectGroupsViewModel.ProjectGroupsPaginatedList.SelectedItem =
            projectGroupsViewModel.ProjectGroupsPaginatedList.CurrentPageContent.First();
        projectGroupsViewModel.OpenProjectGroup.Execute().Subscribe();

        var projectsViewModel = (ProjectsViewModel)CurrentViewModel;
        projectsViewModel.NavigateBack.Execute().Subscribe();

        CurrentViewModel.Should().BeOfType<ProjectGroupsViewModel>();
    }

    [Test]
    public void NavigateBackFromTasksViewModel()
    {
        _dataGenerator.AddTestLocalVault();
        _dataGenerator.AddProjectGroupForLocalVault("Test Local Vault");
        _dataGenerator.AddProjectToProjectGroup("Test Project Group");
        MainWindowViewModel.ResetAppForUiTesting.Execute().Subscribe();

        var localVaultSelectViewModel = (LocalVaultSelectViewModel)CurrentViewModel;
        localVaultSelectViewModel.LocalVaultsPaginatedList.SelectedItem =
            localVaultSelectViewModel.LocalVaultsPaginatedList.CurrentPageContent.First();
        localVaultSelectViewModel.OpenSelectedVault.Execute().Subscribe();

        var mainMenuViewModel = (MainMenuViewModel)CurrentViewModel;
        mainMenuViewModel.NavigateToProjectGroupsView.Execute().Subscribe();

        var projectGroupsViewModel = (ProjectGroupsViewModel)CurrentViewModel;
        projectGroupsViewModel.ProjectGroupsPaginatedList.SelectedItem =
            projectGroupsViewModel.ProjectGroupsPaginatedList.CurrentPageContent.First();
        projectGroupsViewModel.OpenProjectGroup.Execute().Subscribe();

        var projectsViewModel = (ProjectsViewModel)CurrentViewModel;
        projectsViewModel.ProjectsPaginatedList.SelectedItem =
            projectsViewModel.ProjectsPaginatedList.CurrentPageContent.First();
        projectsViewModel.OpenProject.Execute().Subscribe();

        var tasksViewModel = (TasksViewModel)CurrentViewModel;
        tasksViewModel.NavigateBack.Execute().Subscribe();

        CurrentViewModel.Should().BeOfType<ProjectsViewModel>();
    }

    [Test]
    public void NavigateBackFromInbox()
    {
        _dataGenerator.AddTestLocalVault();
        MainWindowViewModel.ResetAppForUiTesting.Execute().Subscribe();

        var localVaultSelectViewModel = (LocalVaultSelectViewModel)CurrentViewModel;
        localVaultSelectViewModel.LocalVaultsPaginatedList.SelectedItem =
            localVaultSelectViewModel.LocalVaultsPaginatedList.CurrentPageContent.First();
        localVaultSelectViewModel.OpenSelectedVault.Execute().Subscribe();

        var mainMenuViewModel = (MainMenuViewModel)CurrentViewModel;
        mainMenuViewModel.NavigateToProjectGroupsView.Execute().Subscribe();

        mainMenuViewModel.NavigateToInbox.Execute().Subscribe();

        var inboxViewModel = (InboxViewModel)CurrentViewModel;

        inboxViewModel.NavigateBack.Execute().Subscribe();

        CurrentViewModel.Should().BeOfType<MainMenuViewModel>();
    }
}