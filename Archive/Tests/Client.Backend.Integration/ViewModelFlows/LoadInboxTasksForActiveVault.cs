// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.ViewModels;
using TaskVaultNative.Tests.Client.Backend.Integration.Common;

namespace TaskVaultNative.Tests.Client.Backend.Integration.Flows;

[TestFixture]
public class LoadInboxTasksForActiveVault : ViewModelIntegrationTest
{
    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }

    [Test]
    public void EnsureOnlyInboxTasksForActiveVaultAreLoaded()
    {
        _dataGenerator.AddTestLocalVault("Active Vault");
        _dataGenerator.AddTestLocalVault("Other Vault");
        _dataGenerator.AddTaskToVaultInbox("Active Vault", "Active Vault Task");
        _dataGenerator.AddTaskToVaultInbox("Other Vault", "Other Vault Task");
        MainWindowViewModel.ResetAppForUiTesting.Execute().Subscribe();

        var localVaultSelectViewModel = (LocalVaultSelectViewModel)CurrentViewModel;
        localVaultSelectViewModel.LocalVaultsPaginatedList.FullContent.Count().Should().BeGreaterThan(0);
        localVaultSelectViewModel.LocalVaultsPaginatedList.SelectedItem =
            localVaultSelectViewModel.LocalVaultsPaginatedList.CurrentPageContent.First();
        localVaultSelectViewModel.OpenSelectedVault.Execute().Subscribe();

        var mainMenuViewModel = (MainMenuViewModel)CurrentViewModel;
        mainMenuViewModel.NavigateToInbox.Execute().Subscribe();

        var inboxViewModel = (InboxViewModel)CurrentViewModel;
        var taskNames = inboxViewModel.TasksPaginatedList.CurrentPageContent.Select(task => task.Name);

        taskNames.Should().Contain("Active Vault Task");
        taskNames.Should().NotContain("Other Vault Task");
    }
}