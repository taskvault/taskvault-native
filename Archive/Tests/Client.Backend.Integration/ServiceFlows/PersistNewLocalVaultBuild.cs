// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.IO;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services;
using TaskVaultNative.Tests.Client.Backend.Integration.Common;

namespace TaskVaultNative.Tests.Client.Backend.Integration.ServiceIntegrations;

[TestFixture]
public class PersistNewLocalVaultBuild : IntegrationTest
{
    // Is this test redundant? UI integration test tests creation of local vault, which requires
    // vault builder and persistence of vault-build to be functional.
    // This process could also be tested more generically via a ViewModel integration test.
    // ApplicationSqLiteService is not unit tested because it was determined excessive to test database functionality
    // in isolation.
    // Given the above, this test should either be replaced by a View Model flow test, or discarded with the acknowledgement
    // this functionality is already covered by a UI flow test
    [Test]
    public void SaveVaultBuildToSqLiteDatabaseThenLoad()
    {
        var vaultBuilderService = new LocalVaultBuilderService();
        var dbContextFactory = new TestApplicationDbContextFactory();
        var storageService = new ApplicationSqLiteService(dbContextFactory);

        var TaskVaultDataDirectoryPath =
            Path.Join(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "TaskVault");
        Console.WriteLine($"MyPath: {TaskVaultDataDirectoryPath}");

        vaultBuilderService.StartNewLocalVaultBuild();
        vaultBuilderService.SetVaultName("Test Vault");
        var builtVault = vaultBuilderService.NewLocalVaultBuild;
        storageService.SaveContent<LocalVaultModel>(builtVault);

        var loadedVault = storageService.LoadVaultByName("Test Vault");

        loadedVault.Name.Should().Be("Test Vault");
    }
}