// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services;

namespace TaskVaultNative.Tests;

public class TestDataGenerator
{
    private IApplicationDataStorageService _applicationDataStorage;
    private readonly IDbContextFactory<ApplicationDbContext> _applicationDbContextFactory;
    private IConfiguration? _appTestConfiguration;
    private IServiceProvider _testServiceProvider;

    public TestDataGenerator()
    {
        ConfigureAppTestConfiguration();
        ConfigureTestServiceContainer();
        _applicationDbContextFactory = _testServiceProvider.GetService<IDbContextFactory<ApplicationDbContext>>();
        ConfigureApplicationDatabase();
        ConfigureApplicationDataStorageService();
    }

    private void ConfigureAppTestConfiguration()
    {
        _appTestConfiguration = new AppConfigurationLoader("appsettings.test.json").AppConfiguration;
    }

    private void ConfigureTestServiceContainer()
    {
        _testServiceProvider = new ServiceContainer(_appTestConfiguration);
    }

    private void ConfigureApplicationDatabase()
    {
        using (var context = _applicationDbContextFactory.CreateDbContext())
        {
            //context.Database.Migrate();
        }
    }

    private void ConfigureApplicationDataStorageService()
    {
        _applicationDataStorage = _testServiceProvider.GetService<IApplicationDataStorageService>();
    }

    public void B_AddTestLocalVault(string localVaultName)
    {
        if (localVaultName == null) throw new ArgumentNullException();

        using (var context = _applicationDbContextFactory.CreateDbContext())
        {
            var localVaultQuery = context.LocalVaultModels.Where(vault => vault.Name == localVaultName).ToList();

            if (localVaultQuery.Any()) throw new Exception($"Local vault {localVaultName} already exists");

            var localVault = new LocalVaultModel { Name = localVaultName };

            context.Add(localVault);

            context.SaveChanges();
        }
    }

    public void B_AddProjectGroup(string projectGroupName, string localVaultName)
    {
        if (localVaultName == null || projectGroupName == null) throw new ArgumentNullException();

        using (var context = _applicationDbContextFactory.CreateDbContext())
        {
            var localVaultQuery = context.LocalVaultModels.Where(vault => vault.Name == localVaultName).ToList();
            var localVault = context.LocalVaultModels.Single(model => model.Name == localVaultName);
            if (localVault == null) throw new Exception($"Local vault with name {localVaultName} does not exist");

            var projectGroupQuery =
                context.ProjectGroupModels.Single(projectGroup => projectGroup.Name == projectGroupName);
            if (projectGroupQuery != null) throw new Exception($"Project group {projectGroupName} already exists");

            var projectGroup = new ProjectGroupModel { Name = projectGroupName, LocalVault = localVault };

            context.Add(projectGroup);

            context.SaveChanges();
        }
    }

    public void AddTestLocalVault(string vaultName = "Test Local Vault")
    {
        LocalVaultModel localVault;

        using (var context = _applicationDbContextFactory.CreateDbContext())
        {
            var localVaultQuery = context.LocalVaultModels.Where(vault => vault.Name == vaultName).ToList();

            if (localVaultQuery.Any()) vaultName = $"{vaultName}{localVaultQuery.Count + 1}";

            localVault = new LocalVaultModel { Name = vaultName };

            context.Add(localVault);

            context.SaveChanges();
        }
    }

    public void AddProjectGroupForLocalVault(string localVaultName, string projectGroupName = "Test Project Group")
    {
        LocalVaultModel localVault = null;
        ProjectGroupModel projectGroup = null;
        using (var context = _applicationDbContextFactory.CreateDbContext())
        {
            var query = context.LocalVaultModels.Where(vault => vault.Name == localVaultName);
            if (query.Count().Equals(1)) localVault = query.Single();

            if (localVault == null)
            {
                localVault = new LocalVaultModel { Name = localVaultName };
                context.Add(localVault);
                Console.WriteLine($"Local vault with name {localVaultName} was not found, had to be created");
            }

            var projectGroupQuery =
                context.ProjectGroupModels.Where(projectGroup => projectGroup.Name == projectGroupName);

            if (projectGroupQuery.Count() > 0) projectGroupName = $"{projectGroupName} {projectGroupQuery.Count() + 1}";

            projectGroup = new ProjectGroupModel { Name = projectGroupName, LocalVault = localVault };
            context.Add(projectGroup);
            // TODO: foreign key constraint failed
            context.SaveChanges();
        }

        using (var context = _applicationDbContextFactory.CreateDbContext())
        {
            foreach (var projectGroupModel in context.ProjectGroupModels)
                Console.WriteLine($"Project Group model created by test-data-generator: {projectGroupModel.Name}");
        }
    }

    public void AddProjectToProjectGroup(string projectGroupName, string projectName = "Test Project")
    {
        ProjectGroupModel projectGroup = null;
        ProjectModel project = null;

        using (var context = _applicationDbContextFactory.CreateDbContext())
        {
            var query = context.ProjectGroupModels.Where(projectGroup => projectGroup.Name == projectGroupName);
            if (query.Count().Equals(1)) projectGroup = query.Single();

            if (projectGroup == null)
            {
                AddProjectGroupForLocalVault("Test Local Vault", projectGroupName);
                projectGroup = context.ProjectGroupModels.Single(projectGroup => projectGroup.Name == projectGroupName);
            }

            var projectQuery = context.ProjectModels.Where(project => project.Name == projectName);

            if (projectQuery.Count() > 0) projectName = $"{projectName} {projectQuery.Count() + 1}";

            project = new ProjectModel { Name = projectName, ProjectGroupId = projectGroup.ProjectGroupId };
            context.Add(project);
            context.SaveChanges();
        }
    }

    public void AddTaskToProject(string projectName = "Test Project", string taskName = "Test Task")
    {
        ProjectModel project = null;
        TaskModel task = null;

        using (var context = _applicationDbContextFactory.CreateDbContext())
        {
            var query = context.ProjectModels.Where(project => project.Name == projectName);
            if (query.Count().Equals(1)) project = query.Single();
            // TODO
            project = context.ProjectModels.Single(project => project.Name == projectName);
            if (project == null)
            {
                AddProjectToProjectGroup("Test Project Group", projectName);
                project = context.ProjectModels.Single(project => project.Name == projectName);
            }

            task = new TaskModel
                { Name = taskName, Description = "test task description", ProjectId = project.ProjectId };
            context.Add(task);
            context.SaveChanges();
        }
    }

    public void AddTaskToVaultInbox(string vaultName, string taskName = "Test Task")
    {
        LocalVaultModel localVault = null;
        TaskModel task = null;

        using (var context = _applicationDbContextFactory.CreateDbContext())
        {
            localVault = context.LocalVaultModels.Single(vault => vault.Name == vaultName);

            Console.WriteLine($"Local vault {vaultName}: {localVault}");

            var taskQuery = context.TaskModels.Where(task => task.Name == taskName);

            if (taskQuery.Count() > 0) taskName = $"{taskName}{taskQuery.Count() + 1}";

            task = new TaskModel
                { Name = taskName, Description = "test task description", LocalVaultId = localVault.LocalVaultId };

            context.Add(task);
            context.SaveChanges();
            Console.WriteLine($"{taskName} added to Vault Inbox");
        }
    }

    public void ResetDatabase()
    {
        using (var context = _applicationDbContextFactory.CreateDbContext())
        {
            context.Database.EnsureDeleted();
            context.Database.Migrate();
            foreach (var vaultModel in context.LocalVaultModels)
                Console.WriteLine($"Existing vault model after delete: {vaultModel}");
        }
    }
}