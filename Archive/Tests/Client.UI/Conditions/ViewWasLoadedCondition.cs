// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Threading.Tasks;
using Avalonia.LogicalTree;
using TaskVaultNative.Client.Views;

namespace TaskVaultNative.Tests.Client.UI.Conditions;

public static class ViewWasLoadedCondition<TView> where TView : class
{
    private static MainWindow? MainWindow { get; set; }
    private static TView? _view;

    public static Task<bool> CheckAsync()
    {
        return WaitService.WaitForConditionAsync(() => CheckCondition());
    }

    private static bool CheckCondition()
    {
        MainWindow = AvaloniaAppUnderTest.GetMainWindow();

        return ViewExistsInLogicalTree();
    }

    private static bool ViewExistsInLogicalTree()
    {
        _view = MainWindow.FindLogicalDescendantOfType<TView>();
        return _view != null;
    }
}