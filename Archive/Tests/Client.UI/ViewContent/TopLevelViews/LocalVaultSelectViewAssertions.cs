// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.LogicalTree;
using Avalonia.Threading;
using Avalonia.VisualTree;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Views;
using TaskVaultNative.Tests.Client.UI.Conditions;

namespace TaskVaultNative.Tests.Client.UI.ViewContent.EmbeddedViews;

[TestFixture]
public class LocalVaultSeelctViewAssertions : IntegrationTest
{
    [SetUp]
    public void SetUp()
    {
        ResetAppState();
    }

    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }
    
    [Test]
    public async Task HasExpectedContent()
    {
        await ViewWasLoadedCondition<LocalVaultSelectView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var logo = MainWindow.GetVisualDescendants().OfType<Image>()
                .Single(image => image.Name == "TaskVaultLogo");
            logo.Should().NotBeNull();
        
            var copyright = MainWindow.GetVisualDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "Copyright").Text;
            copyright.Should().NotBeEmpty();
            
            var licenseLogo = MainWindow.GetLogicalDescendants().OfType<Image>()
                .Single(image => image.Name == "GplV3Logo");
            licenseLogo.IsEffectivelyVisible.Should().BeTrue();
            
            var version = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "Version");
            version.Text.Should().NotBeEmpty();
            
            var roadmapAndFeedbackLabel = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "RoadmapAndFeedbackLabel");
            roadmapAndFeedbackLabel.Should().NotBeNull();
            
            var roadmapAndFeedbackUrl = MainWindow.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "RoadmapAndFeedbackUrl");
            roadmapAndFeedbackUrl.Should().NotBeNull();
            
            var newVaultButton = MainWindow.GetVisualDescendants().OfType<Button>()
                .Single(button => button.Name == "NewLocalVaultButton");
            newVaultButton.Content.Should().Be("New Local Vault");
            
            var nextPageOfVaultsButton = GetControlFromLogicTree<Button>("NextPageButton");
            nextPageOfVaultsButton.Content.Should().Be("Next");
            nextPageOfVaultsButton.IsEffectivelyEnabled.Should().BeFalse();
            
            var previousPageOfVaultsButton = GetControlFromLogicTree<Button>("PreviousPageButton");
            previousPageOfVaultsButton.Content.Should().Be("Previous");
            previousPageOfVaultsButton.IsEffectivelyEnabled.Should().BeFalse();
            
            var openVaultButton = GetControlFromLogicTree<Button>("OpenVaultButton");
            openVaultButton.Content.Should().Be("Open Vault");
            openVaultButton.IsEffectivelyEnabled.Should().BeFalse();
            
            var localVaultsListBox = GetControlFromLogicTree<ListBox>("LocalVaultsListBox");
            localVaultsListBox.SelectionMode.Should().Be(SelectionMode.Single);
            
            
        });
        
    }
    
    
}