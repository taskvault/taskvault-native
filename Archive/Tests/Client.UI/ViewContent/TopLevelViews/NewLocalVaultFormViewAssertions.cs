// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.LogicalTree;
using Avalonia.Threading;
using Avalonia.VisualTree;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Views;
using TaskVaultNative.Tests.Client.UI.Conditions;

namespace TaskVaultNative.Tests.Client.UI.ViewContent.TopLevelViews;

[TestFixture]
public class NewLocalVaultFormViewAssertions : IntegrationTest
{
    [SetUp]
    public void SetUp()
    {
        ResetAppState();
    }

    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }

    [Test]
    public async Task HasExpectedContent()
    {
        await ViewWasLoadedCondition<LocalVaultSelectView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var newVaultButton = GetControlFromLogicTree<Button>("NewLocalVaultButton");

            newVaultButton.Command.Execute(null);
        });
        
        await ViewWasLoadedCondition<NewLocalVaultFormView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var viewHeader = GetControlFromLogicTree<TextBlock>("ViewHeader");
            viewHeader.Text.Should().NotBeEmpty();

            var cancelButton = GetControlFromLogicTree<Button>("CancelButton");
            cancelButton.Command.Should().NotBeNull();

            var nextButton = GetControlFromLogicTree<Button>("NextButton");
            nextButton.Content.Should().Be("Next");

            var vaultNameField = GetControlFromLogicTree<TextBox>("VaultNameField");
            vaultNameField.Watermark.Should().NotBeEmpty();

        });

    }
}