// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.LogicalTree;
using Avalonia.Threading;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Views;
using TaskVaultNative.Tests.Client.UI.Conditions;

namespace TaskVaultNative.Tests.Client.UI.ViewContent.TopLevelViews;

[TestFixture]
public class ConnectToRemoteFormViewAssertions : IntegrationTest
{
    [SetUp]
    public void SetUp()
    {
        ResetAppState();
    }

    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }

    [Test]
    public async Task HasExpectedContent()
    {
        await ViewWasLoadedCondition<LocalVaultSelectView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var newVaultButton = GetControlFromLogicTree<Button>("NewLocalVaultButton");

            newVaultButton.Command.Execute(null);
        });
        
        await ViewWasLoadedCondition<NewLocalVaultFormView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var nextButton = GetControlFromLogicTree<Button>("NextButton");
            var vaultNameField = GetControlFromLogicTree<TextBox>("VaultNameField");
            
            vaultNameField.Text = "Test Vault Name";
            
            nextButton.Command.Execute(null);
        });
        
        await ViewWasLoadedCondition<EncryptionAtRestFormView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var declineButton = GetControlFromLogicTree<Button>("DeclineButton");
            declineButton.Command.Execute(null);
        });
        
        await ViewWasLoadedCondition<ConnectToRemoteFormView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var viewHeader = GetControlFromLogicTree<TextBlock>("ViewHeader");
            viewHeader.Text.Should().NotBeEmpty();

            var remoteVaultsDescription = GetControlFromLogicTree<TextBlock>("RemoteVaultsDescription");
            remoteVaultsDescription.Text.Should().NotBeNullOrEmpty();
            remoteVaultsDescription.IsVisible.Should().BeFalse();
            
            var remoteVaultsDescriptionToggle = GetControlFromLogicTree<ToggleButton>("RemoteVaultsDescriptionToggle");
            remoteVaultsDescriptionToggle.IsChecked = true;
            remoteVaultsDescription.IsVisible.Should().BeTrue();
            
            var cancelButton = GetControlFromLogicTree<Button>("CancelButton");
            cancelButton.Content.Should().Be("Cancel");
            cancelButton.Command.Should().NotBeNull();
            
            var backButton = GetControlFromLogicTree<Button>("BackButton");
            backButton.Content.Should().Be("Back");
            backButton.Command.Should().NotBeNull();
            
            var registerNewRemoteVaultButton = GetControlFromLogicTree<Button>("RegisterNewRemoteVaultButton");
            registerNewRemoteVaultButton.Content.Should().Be("Register New Remote Vault");

            var connectToExistingRemoteVaultButton = GetControlFromLogicTree<Button>("ConnectToExistingRemoteVaultButton");
            connectToExistingRemoteVaultButton.Content.Should().Be("Connect to Existing Remote Vault");
            
            var declineButton = GetControlFromLogicTree<Button>("DeclineButton");
            declineButton.Content.Should().Be("Decline");
        });
    }
}