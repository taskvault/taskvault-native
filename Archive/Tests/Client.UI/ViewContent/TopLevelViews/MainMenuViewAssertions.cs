// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.LogicalTree;
using Avalonia.Threading;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Views;
using TaskVaultNative.Client.Views.EmbeddedViews;
using TaskVaultNative.Tests.Client.UI.Conditions;

namespace TaskVaultNative.Tests.Client.UI.ViewContent;

[TestFixture]
public class MainMenuViewAssertions : IntegrationTest
{
    [SetUp]
    public void SetUp()
    {
        ResetAppState();
    }

    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }

    [Test]
    public async Task HasExpectedContent()
    {
        await ViewWasLoadedCondition<LocalVaultSelectView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var newVaultButton = GetControlFromLogicTree<Button>("NewLocalVaultButton");

            newVaultButton.Command.Execute(null);
        });
        
        await ViewWasLoadedCondition<NewLocalVaultFormView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var nextButton = GetControlFromLogicTree<Button>("NextButton");
            var vaultNameField = GetControlFromLogicTree<TextBox>("VaultNameField");
            
            vaultNameField.Text = "Test Vault Name";
            
            nextButton.Command.Execute(null);
        });
        
        await ViewWasLoadedCondition<EncryptionAtRestFormView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var declineButton = GetControlFromLogicTree<Button>("DeclineButton");
            declineButton.Command.Execute(null);
        });
        
        await ViewWasLoadedCondition<ConnectToRemoteFormView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var declineButton = GetControlFromLogicTree<Button>("DeclineButton");

            declineButton.Command.Execute(null);
        });
        
        await ViewWasLoadedCondition<MainMenuView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var viewHeader = GetControlFromLogicTree<TextBlock>("ViewHeader");
            viewHeader.Text.Should().NotBeEmpty();
            viewHeader.Text.Should().EndWith("- Test Vault Name");
            
            var headerMenu = GetControlFromLogicTree<Menu>("HeaderMenu");

            var navigateSubMenu = GetControlFromLogicTree<MenuItem>("NavigationSubMenu", headerMenu);
            navigateSubMenu.Header.Should().Be("Navigate");

            var inboxMenuItem = GetControlFromLogicTree<MenuItem>("InboxMenuItem", navigateSubMenu);
            inboxMenuItem.Header.Should().Be("Inbox");
            
            var mainMenuMenuItem = GetControlFromLogicTree<MenuItem>("MainMenuMenuItem", navigateSubMenu);
            mainMenuMenuItem.Header.Should().Be("Main Menu");
            mainMenuMenuItem.Command.Should().NotBeNull();

            var optionsSubMenu = GetControlFromLogicTree<MenuItem>("OptionsSubMenu", headerMenu);
            optionsSubMenu.Header.Should().Be("Options");

            var lockMenuItem = GetControlFromLogicTree<MenuItem>("LockMenuItem", headerMenu);
            lockMenuItem.Header.Should().Be("Lock Vault");
            
            var vaultSettingsMenuItem = GetControlFromLogicTree<MenuItem>("VaultSettingsMenuItem", optionsSubMenu);
            vaultSettingsMenuItem.Header.Should().Be("Vault Settings");
            
            var switchVaultsMenuItem = GetControlFromLogicTree<MenuItem>("SwitchVaultsMenuItem", optionsSubMenu);
            switchVaultsMenuItem.Header.Should().Be("Switch Vaults");

            var helpSubMenu = GetControlFromLogicTree<MenuItem>("HelpSubMenu", headerMenu);
            helpSubMenu.Header.Should().Be("Help");
            
            var inboxButton = GetControlFromLogicTree<Button>("InboxButton");
            inboxButton.Content.Should().Be("Inbox");
            
            var todayButton = GetControlFromLogicTree<Button>("TodayButton");
            todayButton.Content.Should().Be("Today");
            todayButton.IsEnabled.Should().BeFalse();

            var thisWeekButton = GetControlFromLogicTree<Button>("ThisWeekButton");
            thisWeekButton.Content.Should().Be("This Week");
            thisWeekButton.IsEnabled.Should().BeFalse();
            
            var projectGroupsButton = GetControlFromLogicTree<Button>("ProjectGroupsButton");
            projectGroupsButton.Content.Should().Be("Project Groups");
            
            var actionListsListBox = GetControlFromLogicTree<PaginatedContentListBoxView>("ActionListsListBox");
            var noContentMessage = GetControlFromLogicTree<TextBlock>("NoContentMessage", actionListsListBox);
            noContentMessage.Text.Should().NotBeNullOrEmpty();
            
            var newActionListButton = GetControlFromLogicTree<Button>("NewActionListButton");
            newActionListButton.Content.Should().Be("New Action List");
        });
    }
}