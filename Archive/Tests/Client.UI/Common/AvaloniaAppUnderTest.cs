// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Headless;
using Avalonia.ReactiveUI;
using Avalonia.Threading;
using TaskVaultNative.Client;
using TaskVaultNative.Client.Views;

namespace TaskVaultNative.Tests.Client.UI;

public static class AvaloniaAppUnderTest
{
    public static void Stop()
    {
        var app = GetApp();
        if (app is IDisposable disposable) Dispatcher.UIThread.Post(disposable.Dispose);

        Dispatcher.UIThread.Post(() => app.Shutdown());
    }

    public static MainWindow GetMainWindow()
    {
        return (MainWindow)GetApp().MainWindow;
    }

    public static IClassicDesktopStyleApplicationLifetime GetApp()
    {
        return (IClassicDesktopStyleApplicationLifetime)Application.Current.ApplicationLifetime;
    }

    public static AppBuilder BuildAvaloniaApp()
    {
        return AppBuilder
            .Configure<App>()
            .UsePlatformDetect()
            .UseReactiveUI()
            .UseHeadless();
    }
}