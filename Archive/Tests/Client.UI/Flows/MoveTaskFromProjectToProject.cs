// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.LogicalTree;
using Avalonia.Threading;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Views;
using TaskVaultNative.Client.Views.EmbeddedViews;
using TaskVaultNative.Client.Views.TopLevelViews;
using TaskVaultNative.Tests.Client.UI.Conditions;

namespace TaskVaultNative.Tests.Client.UI.Flows;

[TestFixture]
public class MoveTaskFromProjectToProject : IntegrationTest
{
    [SetUp]
    public async Task SetUp()
    {
        _dataGenerator.AddTestLocalVault("UI Test Vault");
        _dataGenerator.AddTaskToVaultInbox("UI Test Vault");
        _dataGenerator.AddProjectGroupForLocalVault("UI Test Vault");
        _dataGenerator.AddProjectGroupForLocalVault("UI Test Vault");
        _dataGenerator.AddProjectToProjectGroup("Test Project Group");
        _dataGenerator.AddProjectToProjectGroup("Test Project Group 2");
        _dataGenerator.AddTaskToProject();
        ResetAppState();
    }

    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }

    [Test]
    public async Task MoveTaskFromProjectToOtherProject()
    {
        await ViewWasLoadedCondition<LocalVaultSelectView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var localVaultListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "UI Test Vault"));
            localVaultListItem.IsSelected = true;

            var openVaultButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenVaultButton");
            openVaultButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<MainMenuView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var projectGroupsButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "ProjectGroupsButton");
            projectGroupsButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<ProjectGroupsView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var projectGroupListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Project Group"));
            projectGroupListItem.Should().NotBeNull();
            projectGroupListItem.IsSelected = true;

            var openProjectGroupButton = projectGroupListItem.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenButton");
            openProjectGroupButton.Content.Should().Be("Open");

            openProjectGroupButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<ProjectsView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var detailsScreen = MainWindow.GetLogicalDescendants().OfType<EmbeddedDetailsScreenView>().Single();
            detailsScreen.Should().NotBeNull();

            var projectListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>().Single(listBoxItem =>
                listBoxItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Project"));
            projectListItem.Should().NotBeNull();
            projectListItem.IsSelected = true;

            var openProjectButton = projectListItem.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenButton");
            openProjectButton.Content.Should().Be("Open");

            openProjectButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<TasksView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var tasksListBox = MainWindow.GetLogicalDescendants().OfType<PaginatedContentListBoxView>()
                .Single(listBox => listBox.Name == "TasksPaginatedListBox");
            var tasksListBoxInternal = tasksListBox.GetLogicalDescendants().OfType<ListBox>().Single();
            var taskListItem = tasksListBoxInternal.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(textBlock => textBlock.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Task"));

            taskListItem.IsSelected = true;

            var taskDetailsView = MainWindow.GetLogicalDescendants().OfType<TaskDetailsView>().Single();

            var moveTaskButton = taskDetailsView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "MoveButton");
            moveTaskButton.Content.Should().Be("Move");

            moveTaskButton.Command.Execute(null);
        });

        await Task.Delay(500);

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var moveTaskView = MainWindow.GetLogicalDescendants().OfType<MoveTaskView>().Single();

            var header = moveTaskView.GetLogicalDescendants().OfType<TextBlock>()
                .Single(header => header.Name == "MoveTaskHeader");
            header.Text.Should().Be("Move Task to Project");

            var projectsPaginatedListBox =
                moveTaskView.GetLogicalDescendants().OfType<PaginatedContentListBoxView>().Single();
            var projectListItem = projectsPaginatedListBox.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(textBlock => textBlock.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Project 2"));

            projectListItem.IsSelected = true;

            var moveButtonOnForm = moveTaskView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "MoveButton");
            moveButtonOnForm.Content.Should().Be("Move");

            moveButtonOnForm.Command.Execute(null);

            var tasksPaginatedListBox = MainWindow.GetLogicalDescendants().OfType<PaginatedContentListBoxView>()
                .Single(listBox => listBox.Name == "TasksPaginatedListBox");
            var taskListItemQuery = tasksPaginatedListBox.GetLogicalDescendants().OfType<ListBoxItem>()
                .Where(listBoxItem => listBoxItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Name == "Test Task"));
            taskListItemQuery.Should().BeEmpty();

            var headerMenu = MainWindow.GetLogicalDescendants().OfType<VaultHeaderMenuView>().Single();
            var mainMenuMenuItem = headerMenu.GetLogicalDescendants().OfType<MenuItem>()
                .Single(menuItem => menuItem.Name == "MainMenuMenuItem");

            mainMenuMenuItem.Command.Execute(null);
        });

        await ViewWasLoadedCondition<MainMenuView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var projectGroupsButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "ProjectGroupsButton");
            projectGroupsButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<ProjectGroupsView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var projectGroupListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Project Group 2"));
            projectGroupListItem.Should().NotBeNull();
            projectGroupListItem.IsSelected = true;

            var openProjectGroupButton = projectGroupListItem.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenButton");
            openProjectGroupButton.Content.Should().Be("Open");

            openProjectGroupButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<ProjectsView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var detailsScreen = MainWindow.GetLogicalDescendants().OfType<EmbeddedDetailsScreenView>().Single();
            detailsScreen.Should().NotBeNull();

            var projectListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>().Single(listBoxItem =>
                listBoxItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Project 2"));
            projectListItem.Should().NotBeNull();

            projectListItem.IsSelected = true;

            var openProjectButton = projectListItem.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenButton");
            openProjectButton.Content.Should().Be("Open");

            openProjectButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<TasksView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var tasksListBox = MainWindow.GetLogicalDescendants().OfType<PaginatedContentListBoxView>()
                .Single(listBox => listBox.Name == "TasksPaginatedListBox");
            var tasksListBoxInternal = tasksListBox.GetLogicalDescendants().OfType<ListBox>().Single();
            var taskListItem = tasksListBoxInternal.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(textBlock => textBlock.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Task"));

            taskListItem.Should().NotBeNull();
        });
    }
}