// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.LogicalTree;
using Avalonia.Threading;
using Avalonia.VisualTree;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Views;
using TaskVaultNative.Client.Views.EmbeddedViews;
using TaskVaultNative.Tests.Client.UI.Conditions;

namespace TaskVaultNative.Tests.Client.UI.Flows;

[TestFixture]
[NonParallelizable]
public class CreateProject : IntegrationTest
{
    [SetUp]
    public void SetUp()
    {
        _dataGenerator.AddTestLocalVault();
        _dataGenerator.AddProjectGroupForLocalVault("Test Local Vault");
        ResetAppState();
    }

    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }

    [Test]
    public async Task CreateProjectInExistingProjectGroup()
    {
        await ViewWasLoadedCondition<LocalVaultSelectView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var localVaultListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Local Vault"));
            localVaultListItem.IsSelected = true;

            var openVaultButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenVaultButton");
            openVaultButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<MainMenuView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var projectGroupsButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "ProjectGroupsButton");
            projectGroupsButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<ProjectGroupsView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var projectGroupListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Project Group"));
            projectGroupListItem.Should().NotBeNull();
            projectGroupListItem.IsSelected = true;

            var openProjectGroupButton = projectGroupListItem.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenButton");
            openProjectGroupButton.Content.Should().Be("Open");

            openProjectGroupButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<ProjectsView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var projectsHeaderTextBlock = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "ProjectsHeader");
            projectsHeaderTextBlock.Text.Should().Be("Projects - Test Project Group");

            var projectsListBox = MainWindow.GetLogicalDescendants().OfType<PaginatedContentListBoxView>()
                .Single(listBox => listBox.Name == "ProjectsListBox");
            var projectsListBoxInternal = projectsListBox.GetLogicalDescendants().OfType<ListBox>().Single();
            projectsListBoxInternal.SelectionMode.Should().Be(SelectionMode.Single);
            projectsListBoxInternal.IsVisible.Should().BeFalse();

            var noProjectMessage = projectsListBox.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoContentMessage");
            noProjectMessage.IsVisible.Should().BeTrue();
            noProjectMessage.Text.Should().NotBeEmpty();

            var backButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "BackButton");
            backButton.Content.Should().Be("Back");
            backButton.Command.Should().NotBeNull();

            var nextPageOfProjectsButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "NextPageButton");
            nextPageOfProjectsButton.Content.Should().Be("Next");
            nextPageOfProjectsButton.IsEffectivelyEnabled.Should().BeFalse();

            var previousPageOfProjectsButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "PreviousPageButton");
            previousPageOfProjectsButton.Content.Should().Be("Previous");
            previousPageOfProjectsButton.IsEffectivelyEnabled.Should().BeFalse();

            var detailsView = MainWindow.GetLogicalDescendants().OfType<EmbeddedDetailsScreenView>().Single();
            detailsView.Should().NotBeNull();

            var noContentMessage = detailsView.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoContentMessage");
            noContentMessage.Text.Should().NotBeEmpty();

            var newProjectButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "NewProjectButton");
            newProjectButton.Content.Should().Be("New Project");

            newProjectButton.Command.Execute(null);

            var newProjectFormView = MainWindow.GetVisualDescendants().OfType<NewProjectFormView>()
                .Single();

            var newProjectFormHeader = newProjectFormView.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "FormHeader");
            newProjectFormHeader.Text.Should().NotBeNullOrEmpty();

            var projectNameField = newProjectFormView.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "ProjectNameField");

            var cancelButton = newProjectFormView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "CancelButton");
            cancelButton.Content.Should().Be("Cancel");
            cancelButton.Command.Should().NotBeNull();

            var submitButton = newProjectFormView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "SubmitButton");
            submitButton.Content.Should().Be("Submit");

            var formErrorMessages = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "FormErrorMessages");
            formErrorMessages.Text.Should().BeNullOrEmpty();
            submitButton.Command.Execute(null);
            formErrorMessages.Text.Should().NotBeEmpty();

            projectNameField.Text = "Test Project";
            formErrorMessages.Text.Should().BeEmpty();

            submitButton.Command.Execute(null);
            MainWindow.GetVisualDescendants().OfType<NewProjectFormView>().Count().Should().Be(0);

            noProjectMessage.IsVisible.Should().BeFalse();
            projectsListBoxInternal.IsVisible.Should().BeTrue();
        });

        await Task.Delay(500);

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var projectListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>().Single(listBoxItem =>
                listBoxItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Project"));
            projectListItem.Should().NotBeNull();
        });
    }
}