// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.LogicalTree;
using Avalonia.Threading;
using Avalonia.VisualTree;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Views;
using TaskVaultNative.Client.Views.EmbeddedViews;
using TaskVaultNative.Client.Views.TopLevelViews;
using TaskVaultNative.Tests.Client.UI.Conditions;

namespace TaskVaultNative.Tests.Client.UI.Flows;

[TestFixture]
[NonParallelizable]
public class CreateTask : IntegrationTest
{
    [SetUp]
    public void SetUp()
    {
        _dataGenerator.AddTestLocalVault();
        _dataGenerator.AddProjectGroupForLocalVault("Test Local Vault");
        _dataGenerator.AddProjectToProjectGroup("Test Project Group");
        ResetAppState();
    }

    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }

    [Test]
    public async Task CreateNewTaskWithNoContextsForExistingProjectAndViewDetails()
    {
        await ViewWasLoadedCondition<LocalVaultSelectView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var localVaultListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Local Vault"));
            localVaultListItem.IsSelected = true;

            var openVaultButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenVaultButton");
            openVaultButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<MainMenuView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var projectGroupsButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "ProjectGroupsButton");
            projectGroupsButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<ProjectGroupsView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var projectGroupListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Project Group"));
            projectGroupListItem.Should().NotBeNull();
            projectGroupListItem.IsSelected = true;

            var openProjectGroupButton = projectGroupListItem.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenButton");
            openProjectGroupButton.Content.Should().Be("Open");

            openProjectGroupButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<ProjectsView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var detailsScreen = MainWindow.GetLogicalDescendants().OfType<EmbeddedDetailsScreenView>().Single();
            detailsScreen.Should().NotBeNull();

            var projectListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>().Single(listBoxItem =>
                listBoxItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Project"));
            projectListItem.Should().NotBeNull();
            projectListItem.IsSelected = true;

            var openProjectButton = projectListItem.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenButton");
            openProjectButton.Content.Should().Be("Open");

            openProjectButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<TasksView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var tasksHeaderTextBlock = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "TasksHeader");
            tasksHeaderTextBlock.Text.Should().Be("Tasks - Test Project");

            var tasksListBox = MainWindow.GetLogicalDescendants().OfType<PaginatedContentListBoxView>()
                .Single(listBox => listBox.Name == "TasksPaginatedListBox");
            var tasksListBoxInternal = tasksListBox.GetLogicalDescendants().OfType<ListBox>().Single();
            tasksListBoxInternal.SelectionMode.Should().Be(SelectionMode.Single);
            tasksListBoxInternal.IsVisible.Should().BeFalse();

            var noTasksMessage = tasksListBox.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoContentMessage");
            noTasksMessage.IsVisible.Should().BeTrue();
            noTasksMessage.Text.Should().NotBeEmpty();

            var backButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "BackButton");
            backButton.Content.Should().Be("Back");
            backButton.Command.Should().NotBeNull();

            var nextPageButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "NextPageButton");
            nextPageButton.Content.Should().Be("Next");
            nextPageButton.IsEffectivelyEnabled.Should().BeFalse();

            var previousPageButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "PreviousPageButton");
            previousPageButton.Content.Should().Be("Previous");
            previousPageButton.IsEffectivelyEnabled.Should().BeFalse();

            var detailsView = MainWindow.GetLogicalDescendants().OfType<EmbeddedDetailsScreenView>().Single();
            detailsView.Should().NotBeNull();

            var noContentMessage = detailsView.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoContentMessage");
            noContentMessage.Text.Should().NotBeEmpty();

            var newTaskButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "NewTaskButton");
            newTaskButton.Content.Should().Be("New Task");

            newTaskButton.Command.Execute(null);
        });

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var newTaskFormView = MainWindow.GetVisualDescendants().OfType<NewTaskFormView>().Single();
            newTaskFormView.Should().NotBeNull();

            var newTaskHeader = newTaskFormView.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "FormHeader");
            newTaskHeader.Text.Should().NotBeNullOrEmpty();

            var taskNameField = newTaskFormView.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "TaskNameField");

            var taskContextField = newTaskFormView.GetLogicalDescendants().OfType<ListBox>()
                .Single(listBox => listBox.Name == "TaskContextField");
            taskContextField.SelectionMode.Should().Be(SelectionMode.Multiple | SelectionMode.Toggle);
            taskContextField.IsVisible.Should().BeFalse();

            var noTaskContextsMessage = newTaskFormView.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoTaskContextsMessage");
            noTaskContextsMessage.Text.Should().NotBeEmpty();
            noTaskContextsMessage.IsVisible.Should().BeTrue();

            var taskDescriptionField = newTaskFormView.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "TaskDescriptionField");
            taskDescriptionField.Watermark.Should().Be("Task Description");

            var cancelButton = newTaskFormView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "CancelButton");
            cancelButton.Content.Should().Be("Cancel");
            cancelButton.Command.Should().NotBeNull();

            var submitButton = newTaskFormView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "SubmitButton");
            submitButton.Content.Should().Be("Submit");

            var formErrorMessages = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "FormErrorMessages");
            formErrorMessages.Text.Should().BeNullOrEmpty();
            submitButton.Command.Execute(null);
            formErrorMessages.Text.Should().NotBeEmpty();

            taskNameField.Text = "Test Task";
            taskDescriptionField.Text = "Task description";
            formErrorMessages.Text.Should().BeEmpty();

            submitButton.Command.Execute(null);
        });

        await Task.Delay(500);

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var detailsScreen = MainWindow.GetLogicalDescendants().OfType<EmbeddedDetailsScreenView>().Single();
            detailsScreen.Should().NotBeNull();

            var noContentMessage = detailsScreen.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoContentMessage");
            noContentMessage.IsEffectivelyVisible.Should().BeTrue();

            var taskListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>().Single(listBoxItem =>
                listBoxItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Task"));
            taskListItem.Should().NotBeNull();
            taskListItem.IsSelected = true;

            var taskDetailsView = MainWindow.GetLogicalDescendants().OfType<TaskDetailsView>().Single();

            var header = taskDetailsView.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "TaskDetailsHeader");
            header.Text.Should().Be("Test Task");

            var taskDescription = taskDetailsView.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "TaskDescription");
            taskDescription.Text.Should().Be("Task description");

            var editButton = taskDetailsView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "EditButton");
            editButton.Content.Should().Be("Edit");

            var deleteButton = taskDetailsView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "DeleteButton");
            deleteButton.Content.Should().Be("Delete");

            var taskContexts = taskDetailsView.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "TaskContexts");
            taskContexts.Text.Should().Be("No contexts");
        });
    }
}