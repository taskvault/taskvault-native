// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.LogicalTree;
using Avalonia.Threading;
using Avalonia.VisualTree;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Views;
using TaskVaultNative.Client.Views.EmbeddedViews;
using TaskVaultNative.Tests.Client.UI.Conditions;

namespace TaskVaultNative.Tests.Client.UI.Flows;

[TestFixture]
[NonParallelizable]
public class CreateLocalVault : IntegrationTest
{
    [SetUp]
    public void SetUp()
    {
        ResetAppState();
    }

    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }

    [Test]
    public async Task CreateLocalVaultWithNoAdditionalConfigurations()
    {
        await ViewWasLoadedCondition<LocalVaultSelectView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var newVaultButton = GetControlFromLogicTree<Button>("NewLocalVaultButton");
            
            var noLocalVaultsMessage = GetControlFromLogicTree<TextBlock>("NoLocalVaultsMessage");
            noLocalVaultsMessage.IsVisible.Should().BeTrue();
            noLocalVaultsMessage.Text.Should().NotBeEmpty();
            
            var localVaultsListBox = GetControlFromLogicTree<ListBox>("LocalVaultsListBox");
            localVaultsListBox.IsVisible.Should().BeFalse();

            newVaultButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<NewLocalVaultFormView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var nextButton = GetControlFromLogicTree<Button>("NextButton");
            var vaultNameField = GetControlFromLogicTree<TextBox>("VaultNameField");
            
            var formErrorMessages = GetControlFromLogicTree<TextBlock>("FormErrorMessages");
            formErrorMessages.Text.Should().BeNullOrEmpty();
            
            // test invalid input
            nextButton.Command.Execute(null);
            formErrorMessages.Text.Should().NotBeEmpty();

            // enter valid input
            vaultNameField.Text = "Test Vault Name";
            nextButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<EncryptionAtRestFormView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var declineButton = GetControlFromLogicTree<Button>("DeclineButton");
            declineButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<ConnectToRemoteFormView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var declineButton = GetControlFromLogicTree<Button>("DeclineButton");

            declineButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<MainMenuView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var switchVaultsMenuItem = GetControlFromLogicTree<MenuItem>("SwitchVaultsMenuItem");

            switchVaultsMenuItem.Command.Execute(null);
        });

        await Task.Delay(1000); // wait needed to ensure local vaults have been loaded from database
        await ViewWasLoadedCondition<LocalVaultSelectView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var openVaultButton = GetControlFromLogicTree<Button>("OpenVaultButton");
            
            var localVaultsListBox = GetControlFromLogicTree<ListBox>("LocalVaultsListBox");
            localVaultsListBox.IsVisible.Should().BeTrue();
            
            var numberOfVaults = localVaultsListBox.GetLogicalDescendants().OfType<ListBoxItem>();
            numberOfVaults.Count().Should().Be(1);
            
            var noLocalVaultsMessage = GetControlFromLogicTree<TextBlock>("NoLocalVaultsMessage");
            noLocalVaultsMessage.IsVisible.Should().BeFalse();

            var createdLocalVault = localVaultsListBox.GetLogicalDescendants().OfType<TextBlock>()
                .Where(textBlock => textBlock.Name == "LocalVaultName")
                .Where(textBlock => textBlock.Text == "Test Vault Name"); //TODO : figure out why Single is failing
            createdLocalVault.Should().NotBeNull();

            var createdLocalVaultListItem =
                GetControlWithChildControlTextFromLogicTree<ListBoxItem, TextBlock>("Test Vault Name");
            createdLocalVaultListItem.IsSelected = true;
            openVaultButton.IsEnabled.Should().BeTrue();

            openVaultButton.Command.Execute(null);
        });
        
        await ViewWasLoadedCondition<MainMenuView>.CheckAsync();
    }
}