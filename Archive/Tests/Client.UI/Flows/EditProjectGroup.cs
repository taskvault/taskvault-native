// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.LogicalTree;
using Avalonia.Threading;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Views;
using TaskVaultNative.Client.Views.EmbeddedViews;
using TaskVaultNative.Tests.Client.UI.Conditions;

namespace TaskVaultNative.Tests.Client.UI.Flows;

[TestFixture]
public class EditProjectGroup : IntegrationTest
{
    [SetUp]
    public void SetUp()
    {
        _dataGenerator.AddTestLocalVault();
        _dataGenerator.AddProjectGroupForLocalVault("Test Local Vault");
        ResetAppState();
    }

    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }

    [Test]
    public async Task EditExistingProjectGroup()
    {
        await ViewWasLoadedCondition<LocalVaultSelectView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var localVaultListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Local Vault"));
            localVaultListItem.IsSelected = true;

            var openVaultButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenVaultButton");
            openVaultButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<MainMenuView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var projectGroupsButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "ProjectGroupsButton");
            projectGroupsButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<ProjectGroupsView>.CheckAsync();
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var detailsView = MainWindow.GetLogicalDescendants().OfType<EmbeddedDetailsScreenView>().Single();
            detailsView.Should().NotBeNull();

            var projectGroupListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Project Group"));
            projectGroupListItem.Should().NotBeNull();
            projectGroupListItem.IsSelected = true;

            var editButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "EditButton");

            editButton.Command.Execute(null);

            var editProjectGroupFormView =
                MainWindow.GetLogicalDescendants().OfType<EditProjectGroupFormView>().Single();
            editProjectGroupFormView.Should().NotBeNull();
        });

        await Task.Delay(600);

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var editProjectGroupFormView =
                MainWindow.GetLogicalDescendants().OfType<EditProjectGroupFormView>().Single();
            var editProjectGroupHeader = editProjectGroupFormView.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "FormHeader");
            editProjectGroupHeader.Text.Should().Be("Editing Project Group - Test Project Group");

            var projectGroupNameField = MainWindow.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "ProjectGroupNameField");
            projectGroupNameField.Text = "Renamed Test Project Group";

            var submitButton = editProjectGroupFormView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "SubmitButton");
            submitButton.Content.Should().Be("Submit");

            submitButton.Command.Execute(null);

            var queryForOriginalProjectGroupName = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Where(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Project Group"));
            queryForOriginalProjectGroupName.Should().BeEmpty();

            var queryForModifiedProjectGroupName = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Where(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Renamed Test Project Group"));
            queryForModifiedProjectGroupName.Count().Should().Be(1);
        });
    }
}