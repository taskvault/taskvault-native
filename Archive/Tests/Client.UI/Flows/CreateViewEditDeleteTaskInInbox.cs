// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.LogicalTree;
using Avalonia.Threading;
using Avalonia.VisualTree;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Views;
using TaskVaultNative.Client.Views.EmbeddedViews;
using TaskVaultNative.Client.Views.TopLevelViews;
using TaskVaultNative.Tests.Client.UI.Conditions;

namespace TaskVaultNative.Tests.Client.UI.Flows;

[TestFixture]
[NonParallelizable]
public class CreateViewEditDeleteTaskInInbox : IntegrationTest
{
    [SetUp]
    public async Task SetUp()
    {
        _dataGenerator.AddTestLocalVault("UI Test Vault");
        ResetAppState();
    }

    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }

    [Test]
    public async Task CreateThenViewDetailsThenEditThenDeleteTaskInInbox()
    {
        await ViewWasLoadedCondition<LocalVaultSelectView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var localVaultListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "UI Test Vault"));
            localVaultListItem.IsSelected = true;

            var openVaultButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenVaultButton");
            openVaultButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<MainMenuView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var inboxButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "InboxButton");

            inboxButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<InboxView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var header = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "InboxHeader");
            header.Text.Should().Be("Inbox");

            var backButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "BackButton");
            backButton.Content.Should().Be("Back");
            backButton.Command.Should().NotBeNull();

            var tasksListBox = MainWindow.GetLogicalDescendants().OfType<PaginatedContentListBoxView>()
                .Single(listBox => listBox.Name == "TasksPaginatedListBox");
            var tasksListBoxInternal = tasksListBox.GetLogicalDescendants().OfType<ListBox>().Single();
            tasksListBoxInternal.SelectionMode.Should().Be(SelectionMode.Single);
            tasksListBoxInternal.IsVisible.Should().BeFalse();

            var noTasksMessage = tasksListBox.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoContentMessage");
            noTasksMessage.IsVisible.Should().BeTrue();
            noTasksMessage.Text.Should().NotBeEmpty();

            var nextPageButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "NextPageButton");
            nextPageButton.IsEffectivelyEnabled.Should().BeFalse();

            var previousPageButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "PreviousPageButton");
            previousPageButton.IsEffectivelyEnabled.Should().BeFalse();

            var detailsView = MainWindow.GetLogicalDescendants().OfType<EmbeddedDetailsScreenView>().Single();
            detailsView.Should().NotBeNull();

            var noContentMessage = detailsView.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoContentMessage");
            noContentMessage.Text.Should().NotBeEmpty();

            var newTaskButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "NewTaskButton");

            newTaskButton.Command.Execute(null);
        });

        await Task.Delay(500);

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var newTaskFormView = MainWindow.GetVisualDescendants().OfType<NewTaskFormView>().Single();
            newTaskFormView.Should().NotBeNull();

            var taskNameField = newTaskFormView.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "TaskNameField");
            taskNameField.Should().NotBeNull();

            var taskDescriptionField = newTaskFormView.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "TaskDescriptionField");
            taskDescriptionField.Should().NotBeNull();

            taskNameField.Text = "Test Inbox Task";
            taskDescriptionField.Text = "Task Inbox description";

            var submitButton = newTaskFormView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "SubmitButton");
            submitButton.Should().NotBeNull();

            submitButton.Command.Execute(null);
        });

        await Task.Delay(500);

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var detailsScreen = MainWindow.GetLogicalDescendants().OfType<EmbeddedDetailsScreenView>().Single();
            detailsScreen.Should().NotBeNull();

            var newTaskFormViews = detailsScreen.GetLogicalDescendants().OfType<NewTaskFormView>().ToList();
            newTaskFormViews.Should().BeEmpty();

            var noContentMessage = detailsScreen.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoContentMessage");
            noContentMessage.IsEffectivelyVisible.Should().BeTrue();

            var tasksListBox = MainWindow.GetLogicalDescendants().OfType<PaginatedContentListBoxView>().Single();
            var noTasksMessage = tasksListBox.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "NoContentMessage");
            noTasksMessage.IsVisible.Should().BeFalse();

            var taskListItems = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>();
            taskListItems.Count().Should().Be(1);

            var taskListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>().Single(listBoxItem =>
                listBoxItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Inbox Task"));
            taskListItem.Should().NotBeNull();

            taskListItem.IsSelected = true;

            var taskDetailsView = MainWindow.GetLogicalDescendants().OfType<TaskDetailsView>().Single();

            var editButton = taskDetailsView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "EditButton");

            editButton.Command.Execute(null);
        });

        await Task.Delay(500);

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var editTaskFormView =
                MainWindow.GetLogicalDescendants().OfType<EditTaskFormView>().Single();

            var editTaskHeader = editTaskFormView.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "FormHeader");
            editTaskHeader.Text.Should().Be("Editing Task - Test Inbox Task");

            var taskNameField = MainWindow.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "TaskNameField");

            taskNameField.Text = "Renamed Test Inbox Task";

            var submitButton = editTaskFormView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "SubmitButton");
            submitButton.Content.Should().Be("Submit");

            submitButton.Command.Execute(null);

            var queryForOriginalTaskName = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Where(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Inbox Task"));
            queryForOriginalTaskName.Should().BeEmpty();

            var modifiedTask = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Renamed Test Inbox Task"));

            modifiedTask.IsSelected = true;

            var taskDetailsView = MainWindow.GetLogicalDescendants().OfType<TaskDetailsView>().Single();

            var deleteButton = taskDetailsView.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "DeleteButton");
            deleteButton.Content.Should().Be("Delete");

            deleteButton.Command.Execute(null);
        });

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var queryForModifiedTaskName = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Where(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Renamed Test Inbox Task"));
            queryForModifiedTaskName.Should().BeEmpty();
        });
    }
}