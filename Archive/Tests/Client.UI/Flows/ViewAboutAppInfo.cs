// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Linq;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.LogicalTree;
using Avalonia.Threading;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Views;
using TaskVaultNative.Client.Views.TopLevelViews;
using TaskVaultNative.Tests.Client.UI.Conditions;

namespace TaskVaultNative.Tests.Client.UI.Flows;

[TestFixture]
public class ViewAboutAppInfo : IntegrationTest
{
    [SetUp]
    public void SetUp()
    {
        _dataGenerator.AddTestLocalVault();
        _dataGenerator.AddProjectGroupForLocalVault("Test Local Vault");
        ResetAppState();
    }
    
    [Test]
    public async Task ViewAboutAppInfoViaVaultHeaderMenuNavigation()
    {
        await ViewWasLoadedCondition<LocalVaultSelectView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var localVaultListItem = MainWindow.GetLogicalDescendants().OfType<ListBoxItem>()
                .Single(listItem => listItem.GetLogicalDescendants().OfType<TextBlock>()
                    .Any(textBlock => textBlock.Text == "Test Local Vault"));
            localVaultListItem.IsSelected = true;

            var aboutButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "AboutButton");
            aboutButton.Content.Should().Be("About");
            aboutButton.Command.Should().NotBeNull();
                
            var openVaultButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "OpenVaultButton");
            openVaultButton.Command.Execute(null);
        });

        await ViewWasLoadedCondition<MainMenuView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var headerMenu = MainWindow.GetLogicalDescendants().OfType<VaultHeaderMenuView>().Single();
            var aboutMenuItem = headerMenu.GetLogicalDescendants().OfType<MenuItem>()
                .Single(menuItem => menuItem.Name == "AboutMenuItem");
            
            aboutMenuItem.Command.Execute(null);
        });
        
        await ViewWasLoadedCondition<AboutView>.CheckAsync();

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            var appLogo = MainWindow.GetLogicalDescendants().OfType<Image>()
                .Single(image => image.Name == "TaskVaultLogo");
            appLogo.Should().NotBeNull();

            var version = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "Version");
            version.Text.Should().NotBeEmpty();
            
            var developerCredit = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "DeveloperCredit");
            developerCredit.Text.Should().NotBeNullOrEmpty();
            
            var developerWebsiteUrl = MainWindow.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "DeveloperWebsiteUrl");
            developerWebsiteUrl.Text.Should().NotBeNullOrEmpty();

            var brandingsGraphicCredit = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "BrandingGraphicsCredit");
            brandingsGraphicCredit.Text.Should().NotBeNullOrEmpty();
            
            var sourceCodeReferenceLabel = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "SourceCodeReferenceLabel");
            sourceCodeReferenceLabel.Text.Should().NotBeNullOrEmpty();
            
            var sourceCodeReferenceUrl = MainWindow.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "SourceCodeReferenceUrl");
            sourceCodeReferenceUrl.Text.Should().NotBeNullOrEmpty();

            var projectSupportPlatformReferenceLabel = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "ProjectSupportPlatformReferenceLabel");
            projectSupportPlatformReferenceLabel.Text.Should().NotBeNullOrEmpty();
            
            var projectSupportPlatformReferenceUrl = MainWindow.GetLogicalDescendants().OfType<TextBox>()
                .Single(textBox => textBox.Name == "ProjectSupportPlatformReferenceUrl");
            projectSupportPlatformReferenceUrl.Text.Should().NotBeNullOrEmpty();

            var copyright = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "Copyright");
            copyright.Text.Should().NotBeNullOrEmpty();

            var licenseNotice = MainWindow.GetLogicalDescendants().OfType<TextBlock>()
                .Single(textBlock => textBlock.Name == "LicenseNotice");
            licenseNotice.Text.Should().NotBeNullOrEmpty();
            
            var backButton = MainWindow.GetLogicalDescendants().OfType<Button>()
                .Single(button => button.Name == "BackButton");
            backButton.Content.Should().Be("Back");
            
            backButton.Command.Execute(null);
        });
        
        await ViewWasLoadedCondition<MainMenuView>.CheckAsync();
    }
    
    [TearDown]
    public void TearDown()
    {
        _dataGenerator.ResetDatabase();
    }
}