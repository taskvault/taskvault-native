// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using NUnit.Framework;

namespace TaskVaultNative.Tests.Client.Unit.Services;

[TestFixture]
public class ApplicationSqLiteServiceAssertions
{
    /*
        For now it has been determined unit testing this class is unnecessary, as it is responsible for wrapping
        database operations, which would seem to be more effectively tested as part of the general application behavior
        tested via integration tests (both View-Model and View/ UI driven)
    
        This class will be left here so as to:
            1) Enable any unit tests to be written for this service in the future if doing so turns out to be useful
            2) Increase the readability of the code by clarifying the testing intentions for this service class, and to
            reduce any confusion associated with determining how the test suites for this projects are organized
    
        In short, it has been determined more comprehensible to explicitly define 0 tests for this class than to abstain from
        including this class in the unit test project.
    */
}