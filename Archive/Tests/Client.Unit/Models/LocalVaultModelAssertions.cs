// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using FluentAssertions;
using NUnit.Framework;

namespace TaskVaultNative.Client.Models;

[TestFixture]
public class LocalVaultModelAssertions
{
    [Test]
    public void HasVaultName()
    {
        var model = new LocalVaultModel();

        model.Name.Should().BeNull();
    }

    [Test]
    public void HasFlagToIndicateIfEncryptionAtRestIsEnabled()
    {
        var model = new LocalVaultModel();

        model.EncryptionAtRestEnabled.Should().BeNull();
    }

    [Test]
    public void HasId()
    {
        var model = new LocalVaultModel();

        model.LocalVaultId = 1;
    }

    [Test]
    public void HasSyncId()
    {
        var model = new LocalVaultModel();

        model.SyncId.Should().BeNull();
    }

    [Test]
    public void HasProjectGroups()
    {
        var model = new LocalVaultModel();

        model.ProjectGroups.Should().BeNullOrEmpty();
    }

    [Test]
    public void HasNavigationCollectionForUnassignedTasks()
    {
        var model = new LocalVaultModel();

        model.UnassignedTasks.Should().BeNullOrEmpty();
    }

    [Test]
    public void HasPasswordSalt()
    {
        var model = new LocalVaultModel();

        //model.Salt.Should().BeNull();
    }

    [Test]
    public void HasSaltedPasswordHash()
    {
        var model = new LocalVaultModel();
        
        // TODO: what data type for password hash?
        //model.SaltedPasswordHash.Should().Be
    }
}