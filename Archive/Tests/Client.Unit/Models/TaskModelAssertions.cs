// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using FluentAssertions;
using NUnit.Framework;

namespace TaskVaultNative.Client.Models;

[TestFixture]
public class TaskModelAssertions
{
    [Test]
    public void HasId()
    {
        var model = new TaskModel();

        model.TaskId.Should().Be(0);
    }

    [Test]
    public void HasName()
    {
        var model = new TaskModel();

        model.Name.Should().BeNullOrEmpty();
    }

    [Test]
    public void HasNavigationPropertyForParentProject()
    {
        var model = new TaskModel();

        model.Project.Should().BeNull();
    }

    [Test]
    public void CanHaveForeignKeyForParentProject()
    {
        var model = new TaskModel();

        model.ProjectId.Should().BeNull();
    }

    [Test]
    public void HasDescription()
    {
        var model = new TaskModel();

        model.Description.Should().BeNull();
    }

    [Test]
    public void HasNavigationCollectionForContexts()
    {
        var model = new TaskModel();

        model.Contexts.Should().BeNullOrEmpty();
    }

    [Test]
    public void DoesNotRequireProjectId()
    {
        var model = new TaskModel();

        model.ProjectId = null;
    }

    [Test]
    public void CanHaveLocalVaultId()
    {
        var model = new TaskModel();

        model.LocalVaultId.Should().BeNull();
    }

    [Test]
    public void HasNavigationPropertyForLocalVault()
    {
        var model = new TaskModel();

        model.LocalVault.Should().BeNull();
    }

    [Test]
    public void CanBeMarkedAsCompleted()
    {
        var model = new TaskModel();

        model.IsCompleted.Should().BeFalse();
    }
}