// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using ReactiveUI;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services;
using TaskVaultNative.Client.ViewModels;

namespace TaskVaultNative.Client;

public class TestEmbeddedViewModelFactory : IEmbeddedViewModelFactory
{
    public EmbeddedDetailsScreenViewModel CreateEmbeddedDetailsScreenViewModel()
    {
        return null;
    }

    public VaultHeaderMenuViewModel CreateVaultHeaderMenuViewModel(IScreen screen,
        LocalVaultModel activeLocalVault = null)
    {
        return null;
    }

    public NewProjectGroupFormViewModel CreateNewProjectGroupFormViewModel(IScreen screen,
        LocalVaultModel activeLocalVault = null)
    {
        return null;
    }

    public PaginatedContentListBoxViewModel<TContent>
        CreatePaginatedContentListBoxViewModel<TContent>(IScreen screen)
    {
        return null;
    }

    public ProjectGroupDetailsViewModel CreateProjectGroupDetailsViewModel(IScreen screen)
    {
        return null;
    }

    public EditProjectGroupFormViewModel CreateEditProjectGroupFormViewModel(IScreen screen)
    {
        return null;
    }

    public NewProjectFormViewModel CreateNewProjectFormViewModel(IScreen screen,
        ProjectGroupModel activeProjectGroup = null)
    {
        return null;
    }

    public ProjectDetailsViewModel CreateProjectDetailsViewModel(IScreen screen)
    {
        return null;
    }

    public EditProjectFormViewModel CreateEditProjectFormViewModel(IScreen screen)
    {
        return null;
    }

    public NewTaskFormViewModel CreateNewTaskFormViewModel(IScreen screen, LocalVaultModel activeLocalVault = null,
        ProjectModel activeProject = null)
    {
        return null;
    }

    public TaskDetailsViewModel CreateTaskDetailsViewModel(IScreen screen)
    {
        return null;
    }

    public EditTaskFormViewModel CreateEditTaskFormViewModel(IScreen screen)
    {
        return null;
    }

    public MoveTaskViewModel CreateMoveTaskViewModel(IScreen screen, LocalVaultModel activeLocalVault = null)
    {
        return null;
    }
}