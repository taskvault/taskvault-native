// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Generic;
using TaskVaultNative.Client.Models;

namespace TaskVaultNative.Client;

public class TestApplicationDataStorageServiceProjectGroupsWtihProjects : TestApplicationDataStorageService
{
    public override IEnumerable<ProjectGroupModel> LoadProjectGroupsForVault(LocalVaultModel localVaultModel)
    {
        var projectGroupModels = new List<ProjectGroupModel>();
        var index = 0;
        while (index < 30)
        {
            projectGroupModels.Add(new ProjectGroupModel
                { Name = $"Project Group {index}", LocalVault = localVaultModel });
            index += 1;
        }

        index = 0;
        while (index < 30) projectGroupModels[index].Projects.Add(new ProjectModel { Name = $"Test Project {index}" });

        return projectGroupModels;
    }
}