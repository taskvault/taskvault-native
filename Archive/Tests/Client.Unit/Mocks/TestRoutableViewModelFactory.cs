// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using ReactiveUI;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services.RoutableViewModelFactory;
using TaskVaultNative.Client.ViewModels;

namespace TaskVaultNative.Client;

public class TestRoutableViewModelFactory : IRoutableViewModelFactory
{
    public LocalVaultSelectViewModel CreateLocalVaultSelectViewModel(IScreen screen)
    {
        return null;
    }

    public NewLocalVaultFormViewModel CreateNewLocalVaultFormViewModel(IScreen screen)
    {
        return null;
    }

    public EncryptionAtRestFormViewModel CreateEncryptionAtRestFormViewModel(IScreen screen)
    {
        return null;
    }

    public ConnectToRemoteFormViewModel CreateConnectToRemoteFormViewModel(IScreen screen)
    {
        return null;
    }

    public MainMenuViewModel CreateMainMenuViewModel(IScreen screen, LocalVaultModel activeLocalVault = null)
    {
        return null;
    }

    public ProjectGroupsViewModel CreateProjectGroupsViewModel(IScreen screen, LocalVaultModel activeLocalVault = null)
    {
        return null;
    }

    public ProjectsViewModel CreateProjectsViewModel(IScreen screen, ProjectGroupModel activeProjectGroup = null)
    {
        return null;
    }

    public TasksViewModel CreateTasksViewModel(IScreen screen, ProjectModel activeProject = null)
    {
        return null;
    }

    public InboxViewModel CreateInboxViewModel(IScreen screen, LocalVaultModel activeLocalVault = null)
    {
        return null;
    }

    public AboutViewModel CreateAboutViewModel(IScreen screen)
    {
        return null;
    }
}