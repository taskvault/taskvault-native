// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Models;

namespace TaskVaultNative.Client.ViewModels;

[TestFixture]
public class MoveTaskViewModelAssertions
{
    private MoveTaskViewModel CreateMoveTaskViewModel()
    {
        return new(new TestScreen(), new TestEmbeddedViewModelFactory(), new TestApplicationDataStorageService(),
            new LocalVaultModel { Name = "Test Local Vault" });
    }

    [Test]
    public void SpecifiesHeader()
    {
        var viewModel = CreateMoveTaskViewModel();

        viewModel.Header.Should().Be("Move Task to Project");
    }

    [Test]
    public void HasTargetTask()
    {
        var viewModel = CreateMoveTaskViewModel();

        viewModel.TargetTask.Should().BeNull();
    }

    [Test]
    public void HasPaginatedProjectList()
    {
        var viewModel = CreateMoveTaskViewModel();

        viewModel.ProjectsPaginatedList.Should().BeNull();
    }

    [Test]
    public void SpecifiesNoProjectsMessage()
    {
        var viewModel = CreateMoveTaskViewModel();

        viewModel.NoProjectsMessage.Should().Be("No projects available which task can be moved to");
    }

    [Test]
    public void StoresActiveLocalVault()
    {
        var viewModel = CreateMoveTaskViewModel();

        viewModel.ActiveLocalVault.Should().BeOfType<LocalVaultModel>();
    }
}