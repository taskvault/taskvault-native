// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using FluentAssertions;
using NUnit.Framework;
using ReactiveUI;

namespace TaskVaultNative.Client.ViewModels;

[TestFixture]
public class EmbeddedDetailsScreenViewModelAssertions
{
    private EmbeddedDetailsScreenViewModel CreateEmbeddedDetailsScreenViewModel()
    {
        return new(new TestEmbeddedViewModelFactory());
    }

    [Test]
    public void IsARoutingScreen()
    {
        var viewModel = CreateEmbeddedDetailsScreenViewModel();

        viewModel.Should().BeAssignableTo<IScreen>();
    }

    [Test]
    public void SpecifiesDefaultNoContentMessage()
    {
        var viewModel = CreateEmbeddedDetailsScreenViewModel();

        viewModel.NoContentMessage.Should().Be("No content to display");
    }

    [Test]
    public void CanSetCustomNoContentMessage()
    {
        var viewModel = CreateEmbeddedDetailsScreenViewModel();
        var message = "Custom no-content message";

        viewModel.NoContentMessage = message;

        viewModel.NoContentMessage.Should().Be(message);
    }
}