// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Models;

namespace TaskVaultNative.Client.ViewModels;

[TestFixture]
public class NewProjectGroupFormViewModelAssertions
{
    private NewProjectGroupFormViewModel CreateNewProjectFormViewModel()
    {
        return new(new TestScreen(), new TestApplicationDataStorageService());
    }

    [Test]
    public void HasHeader()
    {
        var viewModel = CreateNewProjectFormViewModel();

        viewModel.Header.Should().Be("New Project Group");
    }

    [Test]
    public void RaisesValidationErrorIfNameIsBlank()
    {
        var viewModel = CreateNewProjectFormViewModel();
        var invalidInput = string.Empty;

        viewModel.ProjectGroupName = invalidInput;
        viewModel.SubmitForm.Execute().Subscribe();

        viewModel.ErrorMessages.Should().Contain("Project Group name cannot be blank");
    }

    [Test]
    public void RaisesValidationErrorIfNameExceeds256Characters()
    {
        var viewModel = CreateNewProjectFormViewModel();
        var invalidInput = "3Z4dXM7MA8FqZwwQ2caFgGqenqTrX9fbAAnN8UBJPTTXY2eHTdRAER5qQaSPzKhu7ixCWZF7LL" +
                           "MNLP3HSnCPWJf4BVkJpW2yMRvASDbuYmFGbhV98TdqxN5CB6pzqhXngTHDvC3vBAyaJUGvM2jRTqmYSM" +
                           "JcziLN9Rh4wdkJTvBSmQ9ntYnAUmJea78cyQ5tMpLr3eJchNz4ui6AGSUwBv29LWbRYyb854QbhNNAvN" +
                           "zUvdLmnHmnZmrKzhRwQc257";

        viewModel.ProjectGroupName = invalidInput;
        viewModel.SubmitForm.Execute().Subscribe();

        viewModel.ErrorMessages.Should().Contain("Project Group name cannot exceed 256 characters");
    }

    [Test]
    public void RaisesValidationErrorIfNameUsesInvalidCharacters()
    {
        var viewModel = CreateNewProjectFormViewModel();
        var invalidInput = "#invalid name";

        viewModel.ProjectGroupName = invalidInput;
        viewModel.SubmitForm.Execute().Subscribe();

        viewModel.ErrorMessages.Should()
            .Contain("Project Group name may include the following characters:\n A-Z a-z 0-9 . - _ WHITESPACE");
    }

    [Test]
    public void StoresActiveLocalVault()
    {
        var viewModel = CreateNewProjectFormViewModel();

        viewModel.ActiveLocalVault.Should().BeOfType<LocalVaultModel>();
    }
}