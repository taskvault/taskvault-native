// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using FluentAssertions;
using NUnit.Framework;
using ReactiveUI;
using ReactiveUI.Validation.Abstractions;
using TaskVaultNative.Client.Services;

namespace TaskVaultNative.Client.ViewModels;

[TestFixture]
public class NewLocalVaultFormViewModelAssertions
{
    private NewLocalVaultFormViewModel CreateNewLocalVaultFormViewModel(
        IApplicationDataStorageService testApplicationDataStorageService = null)
    {
        if (testApplicationDataStorageService == null)
            testApplicationDataStorageService = new TestApplicationDataStorageService();

        return new NewLocalVaultFormViewModel(new TestScreen(), new TestRoutableViewModelFactory(),
            new TestLocalVaultBuilderService(),
            testApplicationDataStorageService);
    }

    [Test]
    public void IsRoutable()
    {
        var viewModel = CreateNewLocalVaultFormViewModel();
        viewModel.Should().BeAssignableTo<IRoutableViewModel>();
    }

    [Test]
    public void SpecifiesUrl()
    {
        var viewModel = CreateNewLocalVaultFormViewModel();
        viewModel.UrlPathSegment.Should().Be("new-local-vault-form");
    }

    [Test]
    public void SupportsValidations()
    {
        var viewModel = CreateNewLocalVaultFormViewModel();
        viewModel.Should().BeAssignableTo<IValidatableViewModel>();
    }

    [Test]
    public void AcceptsValidVaultName()
    {
        var viewModel = CreateNewLocalVaultFormViewModel();
        var validInput = "demo valid vault name";
        viewModel.Activator.Activate();

        viewModel.LocalVaultName = validInput;
        viewModel.ErrorMessages.Should().BeNullOrEmpty();
    }

    [Test]
    public void RaisesValidationErrorIfVaultNameIsBlank()
    {
        var viewModel = CreateNewLocalVaultFormViewModel();
        var invalidInput = string.Empty;

        viewModel.LocalVaultName = invalidInput;

        viewModel.SubmitForm.Execute().Subscribe();

        viewModel.ErrorMessages.Should().Contain("Vault name cannot be blank");
    }

    [Test]
    public void RaisesValidationErrorIfVaultNameExceeds256Characters()
    {
        var viewModel = CreateNewLocalVaultFormViewModel();
        var invalidInput = "3Z4dXM7MA8FqZwwQ2caFgGqenqTrX9fbAAnN8UBJPTTXY2eHTdRAER5qQaSPzKhu7ixCWZF7LL" +
                           "MNLP3HSnCPWJf4BVkJpW2yMRvASDbuYmFGbhV98TdqxN5CB6pzqhXngTHDvC3vBAyaJUGvM2jRTqmYSM" +
                           "JcziLN9Rh4wdkJTvBSmQ9ntYnAUmJea78cyQ5tMpLr3eJchNz4ui6AGSUwBv29LWbRYyb854QbhNNAvN" +
                           "zUvdLmnHmnZmrKzhRwQc257";

        viewModel.LocalVaultName = invalidInput;
        viewModel.SubmitForm.Execute().Subscribe();

        viewModel.ErrorMessages.Should().Contain("Vault name cannot exceed 256 characters");
    }

    [Test]
    public void SubmitFormCommandEnablesValidationErrorMessages()
    {
        var viewModel = CreateNewLocalVaultFormViewModel();

        viewModel.ErrorMessages.Should().BeNullOrEmpty();

        viewModel.SubmitForm.Execute().Subscribe();

        viewModel.ErrorMessages.Should().NotBeEmpty();
    }

    [Test]
    public void PresentsViewHeaderPrompt()
    {
        var viewModel = CreateNewLocalVaultFormViewModel();

        viewModel.HeaderPrompt.Should().Be("Please enter a name for the new vault");
    }

    [Test]
    public void PresentsWatermarkForLocalVaultNameField()
    {
        var viewModel = CreateNewLocalVaultFormViewModel();

        viewModel.LocalVaultNameWatermark.Should().Be("Vault Name");
    }

    [Test]
    public void
        RaisesValidationErrorIfVaultNameIsNotUnique() // does this test also cover vaults are successfully loaded?
    {
        var service = new TestApplicationDataStorageServiceSingleVault();
        var viewModel = CreateNewLocalVaultFormViewModel(service);
        var duplicatedNameInput = "Test Vault";

        viewModel.LocalVaultName = duplicatedNameInput;
        viewModel.SubmitForm.Execute().Subscribe();

        viewModel.ErrorMessages.Should().Contain("Vault name must be unique");
    }
}