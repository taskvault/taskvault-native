// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using FluentAssertions;
using NUnit.Framework;

namespace TaskVaultNative.Client.ViewModels;

[TestFixture]
public class EncryptionAtRestFormViewModelAssertions
{
    public EncryptionAtRestFormViewModel CreateEncryptionAtRestViewModel()
    {
        return new(new TestScreen(), new TestRoutableViewModelFactory(), new TestLocalVaultBuilderService());
    }

    [Test]
    public void PresentsViewHeaderPrompt()
    {
        var viewModel = CreateEncryptionAtRestViewModel();

        viewModel.HeaderPrompt.Should().Be("Configure encryption-at-rest for this local vault?");
    }

    [Test]
    public void SpecifiesUrl()
    {
        var viewModel = CreateEncryptionAtRestViewModel();
        viewModel.UrlPathSegment.Should().Be("encryption-at-rest-form");
    }

    [Test]
    public void PresentsWatermarkForPasswordField()
    {
        var viewModel = CreateEncryptionAtRestViewModel();

        viewModel.PasswordFieldWatermark.Should().Be("Enter Local Vault Password");
    }

    [Test]
    public void PresentsWatermarkForConfirmPasswordField()
    {
        var viewModel = CreateEncryptionAtRestViewModel();

        viewModel.ConfirmPasswordFieldWatermark.Should().Be("Confirm Password");
    }

    [Test]
    public void PresentsDescriptionOfEncryptionAtRest()
    {
        var viewModel = CreateEncryptionAtRestViewModel();
        var descriptionText =
            "\"Encryption-at-rest\" refers to the practice of using an encryption key to encrypt all user data " +
            "before it is saved to system storage. When TaskVault needs to access the data, it is loaded from system storage and " +
            "decrypted in memory. This ensures the data cannot be accessed by unauthorized users when the app is not in use. " +
            "\n\n" +
            "Warning: the encryption key will be derived from the local vault password. If this password is lost, it will not be possible " +
            "to recover the encrypted data. Please ensure the password is not lost by using a secure mechanism such as a password management app. ";

        viewModel.EncryptionAtRestDescription.Should().Match(descriptionText);
    }
}