// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using FluentAssertions;
using NUnit.Framework;

namespace TaskVaultNative.Client.ViewModels;

[TestFixture]
public class ConnectToRemoteFormViewModelAssertions
{
    public ConnectToRemoteFormViewModel CreateConnectToRemoteFormViewModel()
    {
        return new(new TestScreen(), new TestRoutableViewModelFactory(), new TestApplicationDataStorageService(),
            new TestLocalVaultBuilderService());
    }

    [Test]
    public void SpecifiesUrl()
    {
        var viewModel = CreateConnectToRemoteFormViewModel();
        viewModel.UrlPathSegment.Should().Be("connect-to-remote-form");
    }

    [Test]
    public void PresentsViewHeaderPrompt()
    {
        var viewModel = CreateConnectToRemoteFormViewModel();

        viewModel.HeaderPrompt.Should().Be("Connect this local vault to a remote vault?");
    }

    [Test]
    public void PresentsDescriptionOfRemoteVaults()
    {
        var viewModel = CreateConnectToRemoteFormViewModel();
        var descriptionText =
            "\"Remote Vaults\" are a mechanism for storing TaskVault user data online, in order to protect against data loss and enable data synchronization " +
            "between user devices. All data is encrypted client-side before being sent to a remote vault (end-to-end-encryption). Only one remote-vault " +
            "can be connected to a local-vault, but many local-vaults can be connected to the same remote-vault, which enables data synchronization. ";

        viewModel.RemoteVaultsDescription.Should().Match(descriptionText);
    }
}