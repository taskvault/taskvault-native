// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Models;

namespace TaskVaultNative.Client.ViewModels;

[TestFixture]
public class TasksViewModelAssertions
{
    private TasksViewModel CreateTasksViewModel()
    {
        return new(new TestScreen(), new TestEmbeddedViewModelFactory(),
            new TestApplicationDataStorageService(), new ProjectModel { Name = "Test Project" });
    }

    [Test]
    public void SpecifiesHeaderWhichIncludesParentProjectGroupName()
    {
        // note: Project created in TestLocalVaultSessionService
        var viewModel = CreateTasksViewModel();

        viewModel.Header.Should().Be("Tasks - Test Project");
    }

    [Test]
    public void HasEmbeddedDetailsScreen()
    {
        var viewModel = CreateTasksViewModel();

        viewModel.DetailsScreen.Should().BeNull();
    }

    [Test]
    public void HasHeaderMenu()
    {
        var viewModel = CreateTasksViewModel();

        viewModel.HeaderMenu.Should().BeNull();
    }

    [Test]
    public void HasEmbeddedListBox()
    {
        var viewModel = CreateTasksViewModel();

        viewModel.TasksPaginatedList.Should().BeNull();
    }

    [Test]
    public void SpecifiesMessageForEmptyTasksList()
    {
        var viewModel = CreateTasksViewModel();

        viewModel.NoTasksMessage.Should().Be("No tasks defined for this project");
    }

    [Test]
    public void SpecifiesTasksListBoxControlName()
    {
        var viewModel = CreateTasksViewModel();

        viewModel.TasksListBoxControlName.Should().Be("TasksListBox");
    }

    [Test]
    public void ShowCompletedTasksToggleStartsAtFalse()
    {
        var viewModel = CreateTasksViewModel();

        viewModel.ShowCompletedTasks.Should().BeFalse();
    }

    [Test]
    public void SpecifiesActiveProject()
    {
        var viewModel = CreateTasksViewModel();

        viewModel.ActiveProject.Should().NotBeNull();
    }
}