// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using FluentAssertions;
using NUnit.Framework;
using TaskVaultNative.Client.Models;

namespace TaskVaultNative.Client.ViewModels;

[TestFixture]
public class ProjectsViewModelAssertions
{
    public ProjectsViewModel CreateProjectsViewModel()
    {
        return new(new TestScreen(),
            new TestEmbeddedViewModelFactory(), new TestRoutableViewModelFactory(),
            new TestApplicationDataStorageService(),
            new ProjectGroupModel { Name = "Test Project Group" });
    }

    [Test]
    public void SpecifiesHeaderWhichIncludesParentProjectGroupName()
    {
        // note: Project Group created in TestLocalVaultSessionService
        var viewModel = CreateProjectsViewModel();

        viewModel.Header.Should().Be("Projects - Test Project Group");
    }

    [Test]
    public void HasEmbeddedDetailsScreen()
    {
        var viewModel = CreateProjectsViewModel();

        viewModel.DetailsScreen.Should().BeNull();
    }

    [Test]
    public void HasHeaderMenu()
    {
        var viewModel = CreateProjectsViewModel();

        viewModel.HeaderMenu.Should().BeNull();
    }

    [Test]
    public void HasEmbeddedListBox()
    {
        var viewModel = CreateProjectsViewModel();

        viewModel.ProjectsPaginatedList.Should().BeNull();
    }

    [Test]
    public void SpecifiesMessageForEmptyProjectsList()
    {
        var viewModel = CreateProjectsViewModel();

        viewModel.NoProjectsMessage.Should().Be("No projects in this project group");
    }

    [Test]
    public void SpecifiesProjectsListBoxControlName()
    {
        var viewModel = CreateProjectsViewModel();

        viewModel.ProjectsListBoxControlName.Should().Be("ProjectsListBox");
    }

    [Test]
    public void SpecifiesActiveProjectGroup()
    {
        var viewModel = CreateProjectsViewModel();

        viewModel.ActiveProjectGroup.Should().NotBeNull();
    }
}