// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.IO;
using FluentAssertions;
using NUnit.Framework;
using ReactiveUI;

namespace TaskVaultNative.Client.ViewModels;

[TestFixture]
public class LocalVaultSelectViewModelAssertions
{
    private LocalVaultSelectViewModel CreateLocalVaultSelectViewModel()
    {
        return new(new TestScreen(), new TestRoutableViewModelFactory(),
            new TestApplicationDataStorageService(), new TestEmbeddedViewModelFactory());
    }

    [Test]
    public void IsRoutable()
    {
        var viewModel = CreateLocalVaultSelectViewModel();
        viewModel.Should().BeAssignableTo<IRoutableViewModel>();
    }

    [Test]
    public void SpecifiesUrl()
    {
        var viewModel = CreateLocalVaultSelectViewModel();
        viewModel.UrlPathSegment.Should().Be("local-vault-select");
    }

    [Test]
    public void PresentsAppTitle()
    {
        var viewModel = CreateLocalVaultSelectViewModel();

        viewModel.AppTitle.Should().Be("TaskVault");
    }

    [Test]
    public void SpecifiesNoLocalVaultsMessage()
    {
        var viewModel = CreateLocalVaultSelectViewModel();

        viewModel.NoLocalVaultsMessage.Should().Be("No local vaults found. Create a vault to get started.");
    }

    [Test]
    public void PresentsCopyrightInfo()
    {
        var viewModel = CreateLocalVaultSelectViewModel();

        viewModel.Copyright.Should().Match("Copyright 2022 Tyler Hasty");
        //viewModel.Credits.Should().Match("Developed by Tyler Hasty (2022)\nBranding graphics by Erik Hasty");
    }

    [Test]
    public void HasEmbeddedListBox()
    {
        var viewModel = CreateLocalVaultSelectViewModel();

        viewModel.LocalVaultsPaginatedList.Should().BeNull();
    }

    [Test]
    public void SpecifiesNoProjectGroupsMessageControlName()
    {
        var viewModel = CreateLocalVaultSelectViewModel();

        viewModel.NoLocalVaultsMessageControlName.Should().Be("NoLocalVaultsMessage");
    }

    [Test]
    public void SpecifiesProjectGroupsListBoxControlName()
    {
        var viewModel = CreateLocalVaultSelectViewModel();

        viewModel.LocalVaultsListBoxControlName.Should().Be("LocalVaultsListBox");
    }
    
    [Test]
    public void SpecifiesTaskVaultLogoSource()
    {
        var viewModel = CreateLocalVaultSelectViewModel();

        viewModel.TaskVaultLogoSource.Should().Be(Path.ChangeExtension(Path.Join("/Assets", "taskvault-logo"), ".png"));
    }

    [Test]
    public void SpecifiesGplV3LogoSource()
    {
        var viewModel = CreateLocalVaultSelectViewModel();

        viewModel.GplV3LogoSource.Should().Be(Path.ChangeExtension(Path.Join("/Assets", "gplv3-with-text-136x68"), ".png"));
    }
    
    [Test]
    public void SpecifiesAppVersion()
    {
        var viewModel = CreateLocalVaultSelectViewModel();

        viewModel.Version.Should().MatchRegex("v[0-9]+.[0-9]+.[0-9]+");
    }
    
    [Test]
    public void SpecifiesRoadmapAndFeedbackReference()
    {
        var viewModel = CreateLocalVaultSelectViewModel();

        viewModel.RoadmapAndFeedbackLabel.Should().Be("Please give feedback, request and vote on features: ");
        viewModel.RoadmapAndFeedbackUrl.Should().Be("https://taskvault.canny.io/feature-requests");
    }
}