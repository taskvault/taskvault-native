TODO: determine whether testing strategy should be described in ARCHITECTIRE.md
or in this file

## Client UI: VisualContent vs Flows
- if View content depends on the state of the application
apart from the active View Model, then the content should
be validated as part of a flow test (specifically, the flow
which generates that content)
- if View content does NOT depend on state of the application
apart from the active View Model, then that content should be
validated via a View Content test
  - View Content tests are like unit tests for View classes,
which unfortunately cannot be literally unit tested due to the 
architecture of AvaloniaUI
  - the primary motivation for extracting content into these
tests was to remove the ambiguity of where the check for certain
view layer content was being performed; the initial process
turned out to be that content was validated by the flow test of
whichever flow happened to be the first flow in which that content
appeared, even if the content had no other inherent dependency on
that particular flow