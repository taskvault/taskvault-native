// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using ReactiveUI;

namespace TaskVaultNative.Client.Models;

public class TaskModel : ReactiveObject
{
    [Key] public virtual int TaskId { get; set; }

    public virtual string Name { get; set; }
    public virtual string? Description { get; set; }
    public virtual ObservableCollection<TaskContextModel> Contexts { get; set; } = new();
    public virtual int? ProjectId { get; set; }
    public virtual ProjectModel Project { get; set; }
    public virtual int? LocalVaultId { get; set; }
    public virtual LocalVaultModel LocalVault { get; set; }
    public virtual bool IsCompleted { get; set; }
}