// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Runtime.Serialization;

namespace TaskVaultNative.Client.Exceptions;

[Serializable]
public class CannotDetermineFilenameException : Exception
{
    public CannotDetermineFilenameException(string message) : base(message)
    {
    }

    // Ensure Exception is Serializable
    protected CannotDetermineFilenameException(SerializationInfo info, StreamingContext ctxt)
        : base(info, ctxt)
    {
    }
}