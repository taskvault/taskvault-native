// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using ReactiveUI;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services;
using TaskVaultNative.Client.Services.RoutableViewModelFactory;

namespace TaskVaultNative.Client.ViewModels;

public class MainMenuViewModel : ReactiveObject, IRoutableViewModel, IActivatableViewModel
{
    private const string _headerPrefix = "Main Menu";

    private LocalVaultModel _activeLocalVault;
    private readonly IEmbeddedViewModelFactory _embeddedViewModelFactory;
    private readonly ObservableAsPropertyHelper<string> _headerSuffix;

    private readonly IRoutableViewModelFactory _routableViewModelFactory;

    public MainMenuViewModel(IScreen screen, IRoutableViewModelFactory routableViewModelFactory,
        IEmbeddedViewModelFactory embeddedViewModelFactory, LocalVaultModel activeLocalVault = null)
    {
        Activator = new ViewModelActivator();
        HostScreen = screen;

        _routableViewModelFactory = routableViewModelFactory;
        _embeddedViewModelFactory = embeddedViewModelFactory;
        ActiveLocalVault = activeLocalVault ?? new LocalVaultModel();

        HeaderMenu = _embeddedViewModelFactory.CreateVaultHeaderMenuViewModel(HostScreen, activeLocalVault);

        ActionListsPaginatedList =
            _embeddedViewModelFactory.CreatePaginatedContentListBoxViewModel<ActionListModel>(HostScreen);

        this.WhenAnyValue(vm => vm.ActionListsPaginatedList)
            .WhereNotNull()
            .Do(paginatedList => paginatedList.NoContentMessage = NoActionListsMessage)
            .Do(paginatedList => paginatedList.Name = ActionListsListBoxControlName)
            // TODO .Do(_ => Reload())
            .Subscribe();

        _headerSuffix = this.WhenAnyValue(vm => vm.ActiveLocalVault.Name)
            .Select(name => " - " + name)
            .ToProperty(this, vm => vm.Header);

        NavigateToProjectGroupsView = ReactiveCommand.CreateFromObservable(() =>
            HostScreen.Router.Navigate.Execute(
                _routableViewModelFactory.CreateProjectGroupsViewModel(HostScreen, ActiveLocalVault)));

        NavigateToInbox = ReactiveCommand.CreateFromObservable(() =>
            HostScreen.Router.Navigate.Execute(
                _routableViewModelFactory.CreateInboxViewModel(HostScreen, ActiveLocalVault)));

        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public VaultHeaderMenuViewModel HeaderMenu { get; }

    public PaginatedContentListBoxViewModel<ActionListModel> ActionListsPaginatedList { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> NavigateToProjectGroupsView { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> NavigateToInbox { get; }

    public LocalVaultModel ActiveLocalVault
    {
        get => _activeLocalVault;
        private set => this.RaiseAndSetIfChanged(ref _activeLocalVault, value);
    }

    public string Header => _headerPrefix + _headerSuffix.Value;

    public string ActionListsListBoxControlName { get; } = "ActionListsListBox";
    public string NoActionListsMessage { get; } = "No Action Lists created for this vault";
    public ViewModelActivator Activator { get; }
    public string UrlPathSegment { get; } = "main-menu";
    public IScreen HostScreen { get; }
}