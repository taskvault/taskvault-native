// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using ReactiveUI;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services;

namespace TaskVaultNative.Client.ViewModels;

public class TasksViewModel : ReactiveObject, IRoutableViewModel, IActivatableViewModel
{
    private ProjectModel _activeProject = new();
    private readonly IApplicationDataStorageService _applicationDataStorage;

    private readonly IEmbeddedViewModelFactory _embeddedViewModelFactory;
    private string _header = "Tasks - Test Project";
    private readonly string _headerPrefix = "Tasks - ";

    private IEnumerable<TaskModel> _loadedTasks = new List<TaskModel>();

    private bool _showCompletedTasks;


    public TasksViewModel(IScreen screen, IEmbeddedViewModelFactory embeddedViewModelFactory,
        IApplicationDataStorageService applicationDataStorage,
        ProjectModel activeProject = null)
    {
        Activator = new ViewModelActivator();
        HostScreen = screen;

        _embeddedViewModelFactory = embeddedViewModelFactory;
        _applicationDataStorage = applicationDataStorage;
        ActiveProject = activeProject ?? new ProjectModel();

        DetailsScreen = _embeddedViewModelFactory.CreateEmbeddedDetailsScreenViewModel();
        HeaderMenu = _embeddedViewModelFactory
            .CreateVaultHeaderMenuViewModel(HostScreen,
                _applicationDataStorage.LoadLocalVaultForProject(ActiveProject));
        TasksPaginatedList =
            _embeddedViewModelFactory.CreatePaginatedContentListBoxViewModel<TaskModel>(HostScreen);

        this.WhenAnyValue(vm => vm.TasksPaginatedList)
            .WhereNotNull()
            .Do(paginatedList => paginatedList.NoContentMessage = NoTasksMessage)
            .Do(paginatedList => paginatedList.Name = TasksListBoxControlName)
            .Do(_ => ReloadTasks())
            .Subscribe();

        this.WhenAnyValue(vm => vm.ActiveProject)
            .Select(project => project.Name)
            .Do(name => Header = $"{_headerPrefix}{name}")
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(NewTaskFormViewModel))
            .Cast<NewTaskFormViewModel>()
            .Select(newTaskFormViewModel => newTaskFormViewModel.SubmitForm)
            .Select(command => command.IsExecuting)
            .Switch()
            .Do(_ => ReloadTasks())
            .Subscribe();

        // TODO: figure out how to use InvokeCommand operator
        this.WhenAnyValue(vm => vm.TasksPaginatedList.SelectedItem)
            .WhereNotNull()
            .Select(task => task.Name)
            .Do(taskName => { OpenTaskDetails.Execute().Subscribe(); })
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(vm => vm.GetType() == typeof(TaskDetailsViewModel))
            .Cast<TaskDetailsViewModel>()
            .Select(vm => vm)
            .Do(vm => vm.Header = TasksPaginatedList.SelectedItem.Name)
            .Do(vm => vm.TaskDescription = TasksPaginatedList.SelectedItem.Description)
            // TODO .Do(vm => vm.TaskContexts = TasksPaginatedList.SelectedItem.Contexts)
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(TaskDetailsViewModel))
            .Cast<TaskDetailsViewModel>()
            .Do(taskDetailsViewModel => taskDetailsViewModel.OpenEditTaskForm = OpenEditTaskForm)
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(TaskDetailsViewModel))
            .Cast<TaskDetailsViewModel>()
            .Do(taskDetailsViewModel => taskDetailsViewModel.DeleteTask = DeleteSelectedTask)
            .Do(taskDetailsViewModel => taskDetailsViewModel.MarkAsCompleted = MarkTaskAsCompleted)
            .Do(taskDetailsViewModel =>
                taskDetailsViewModel.CanMarkComplete = !TasksPaginatedList.SelectedItem.IsCompleted)
            .Do(taskDetailsViewModel => taskDetailsViewModel.OpenMoveTask = OpenMoveTask)
            .Do(taskDetailsViewModel => taskDetailsViewModel.CanMoveTask = true)
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(EditTaskFormViewModel))
            .Cast<EditTaskFormViewModel>()
            .Select(editTaskFormViewModel => editTaskFormViewModel.SubmitForm)
            .Select(command => command.IsExecuting)
            .Switch()
            .Do(_ => ReloadTasks())
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(EditTaskFormViewModel))
            .Cast<EditTaskFormViewModel>()
            .Do(editProjectGroupFormViewModel =>
                editProjectGroupFormViewModel.TargetTask = TasksPaginatedList.SelectedItem)
            .Subscribe();

        this.WhenAnyValue(vm => vm.ShowCompletedTasks)
            .Skip(1)
            .Do(_ => ReloadTasks())
            .Do(_ => DetailsScreen.Router.NavigationStack.Clear())
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(MoveTaskViewModel))
            .Cast<MoveTaskViewModel>()
            .Do(viewModel => viewModel.TargetTask = TasksPaginatedList.SelectedItem)
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(MoveTaskViewModel))
            .Cast<MoveTaskViewModel>()
            .Select(viewModel => viewModel.MoveTaskToProject)
            .Select(command => command.IsExecuting)
            .Switch()
            .Do(_ => ReloadTasks())
            .Subscribe();

        NavigateBack = ReactiveCommand.CreateFromObservable(() => HostScreen.Router.NavigateBack.Execute());

        OpenNewTaskForm = ReactiveCommand.CreateFromObservable(() =>
            DetailsScreen.Router.NavigateAndReset.Execute(
                _embeddedViewModelFactory.CreateNewTaskFormViewModel(DetailsScreen, activeProject: ActiveProject)));

        OpenTaskDetails = ReactiveCommand.CreateFromObservable(() =>
            DetailsScreen.Router.NavigateAndReset.Execute(
                _embeddedViewModelFactory.CreateTaskDetailsViewModel(DetailsScreen))
        );

        OpenEditTaskForm = ReactiveCommand.CreateFromObservable(() =>
            DetailsScreen.Router.NavigateAndReset.Execute(
                _embeddedViewModelFactory.CreateEditTaskFormViewModel(DetailsScreen)));

        var canDeleteSelectedTask = this.WhenAnyValue(vm => vm.TasksPaginatedList.ItemIsSelected);
        DeleteSelectedTask = ReactiveCommand.Create(RespondToDeleteSelectedTask, canDeleteSelectedTask);

        DeleteSelectedTask.IsExecuting
            .Do(_ => DetailsScreen.Router.NavigationStack.Clear())
            .Subscribe();

        MarkTaskAsCompleted = ReactiveCommand.Create(RespondToMarkTaskAsCompleted);

        MarkTaskAsCompleted.IsExecuting
            .Do(_ => DetailsScreen.Router.NavigationStack.Clear())
            .Subscribe();

        OpenMoveTask = ReactiveCommand.CreateFromObservable(() => DetailsScreen.Router.NavigateAndReset
            .Execute(_embeddedViewModelFactory
                .CreateMoveTaskViewModel(DetailsScreen,
                    _applicationDataStorage.LoadLocalVaultForProject(ActiveProject))));

        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public ReactiveCommand<Unit, IRoutableViewModel> NavigateBack { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> OpenNewTaskForm { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> OpenTaskDetails { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> OpenEditTaskForm { get; }
    public ReactiveCommand<Unit, Unit> DeleteSelectedTask { get; }
    public ReactiveCommand<Unit, Unit> MarkTaskAsCompleted { get; }

    public ReactiveCommand<Unit, IRoutableViewModel> OpenMoveTask { get; }

    public EmbeddedDetailsScreenViewModel DetailsScreen { get; }
    public VaultHeaderMenuViewModel HeaderMenu { get; }
    public PaginatedContentListBoxViewModel<TaskModel> TasksPaginatedList { get; }

    private IEnumerable<TaskModel> LoadedTasks
    {
        get => _loadedTasks;
        set => this.RaiseAndSetIfChanged(ref _loadedTasks, value);
    }

    public string Header
    {
        get => _header;
        set => this.RaiseAndSetIfChanged(ref _header, value);
    }

    public bool ShowCompletedTasks
    {
        get => _showCompletedTasks;
        set => this.RaiseAndSetIfChanged(ref _showCompletedTasks, value);
    }

    public ProjectModel ActiveProject
    {
        get => _activeProject;
        set => this.RaiseAndSetIfChanged(ref _activeProject, value);
    }

    public string NoTasksMessage { get; } = "No tasks defined for this project";
    public string TasksListBoxControlName { get; } = "TasksListBox";
    public ViewModelActivator Activator { get; }
    public string UrlPathSegment { get; } = "tasks";
    public IScreen HostScreen { get; }

    private void ReloadTasks()
    {
        LoadedTasks = _applicationDataStorage.LoadTasksInProject(ActiveProject).ToList();
        if (!ShowCompletedTasks) LoadedTasks = LoadedTasks.Where(task => !task.IsCompleted);
        TasksPaginatedList.FullContent = LoadedTasks.ToList();
    }

    private void RespondToDeleteSelectedTask()
    {
        _applicationDataStorage.DeleteContent(TasksPaginatedList.SelectedItem);
        ReloadTasks();
    }

    private void RespondToMarkTaskAsCompleted()
    {
        // TODO: need to enforce consistency between the way different flows handle data
        // Edit form and New form deal with model internally, but details view relies on parent ViewModel
        // When refactoring, details ViewModel should probably be given a TargetTask property, and use that
        // to derive other information.
        TasksPaginatedList.SelectedItem.IsCompleted = true;
        _applicationDataStorage.SaveContent(TasksPaginatedList.SelectedItem);
        ReloadTasks();
    }
}