// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using ReactiveUI;
using ReactiveUI.Validation.Abstractions;
using ReactiveUI.Validation.Contexts;
using ReactiveUI.Validation.Extensions;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services;
using TaskVaultNative.Client.Services.RoutableViewModelFactory;

namespace TaskVaultNative.Client.ViewModels;

public class NewLocalVaultFormViewModel : ReactiveObject, IRoutableViewModel, IActivatableViewModel,
    IValidatableViewModel
{
    private readonly IApplicationDataStorageService _applicationDataStorage;

    private readonly ObservableAsPropertyHelper<string> _errorMessages;
    private readonly HashSet<string> _existingLocalVaultNames;

    private readonly List<LocalVaultModel> _loadedLocalVaults;
    private readonly ILocalVaultBuilderService _localVaultBuilder;

    private string _localVaultName = string.Empty;

    private bool _validationErrorMessagesVisible;

    public NewLocalVaultFormViewModel(IScreen screen, IRoutableViewModelFactory routableViewModelFactory,
        ILocalVaultBuilderService localVaultBuilder,
        IApplicationDataStorageService applicationDataStorage)
    {
        Activator = new ViewModelActivator();

        HostScreen = screen;
        _localVaultBuilder = localVaultBuilder;
        _applicationDataStorage = applicationDataStorage;

        _loadedLocalVaults = (List<LocalVaultModel>)_applicationDataStorage.LoadLocalVaults();
        _existingLocalVaultNames = _loadedLocalVaults.Select(vault => vault.Name).ToHashSet();

        RegisterFormValidationRules();

        _localVaultBuilder.StartNewLocalVaultBuild();

        NavigateBack = ReactiveCommand.Create(() => { HostScreen.Router.NavigateBack.Execute(); });

        var canNavigateToEncryptionAtRestFormView =
            this.WhenAnyValue(vm => vm.ValidationContext.IsValid);
        NavigateToEncryptionAtRestFormView = ReactiveCommand.CreateFromObservable(() =>
                HostScreen.Router.Navigate.Execute(
                    routableViewModelFactory.CreateEncryptionAtRestFormViewModel(HostScreen)),
            canNavigateToEncryptionAtRestFormView);

        SubmitForm = ReactiveCommand.Create(AttemptToProcessForm);

        _errorMessages = this.WhenAnyValue(vm => vm.ValidationContext.Text,
                vm => vm.ValidationErrorMessagesVisible,
                (messages, flag) => messages)
            .Where(_ => ValidationErrorMessagesVisible)
            .Select(validationText => validationText.ToSingleLine("\n"))
            .ToProperty(this, vm => vm.ErrorMessages);

        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public string ErrorMessages => _errorMessages.Value;

    public string LocalVaultName
    {
        get => _localVaultName;
        set => this.RaiseAndSetIfChanged(ref _localVaultName, value);
    }

    public bool ValidationErrorMessagesVisible
    {
        get => _validationErrorMessagesVisible;
        set => this.RaiseAndSetIfChanged(ref _validationErrorMessagesVisible, value);
    }

    public ReactiveCommand<Unit, Unit> NavigateBack { get; }
    public ReactiveCommand<Unit, Unit> SubmitForm { get; }

    public ReactiveCommand<Unit, IRoutableViewModel> NavigateToEncryptionAtRestFormView { get; }

    public string? HeaderPrompt { get; } = "Please enter a name for the new vault";
    public string? LocalVaultNameWatermark { get; } = "Vault Name";
    public ViewModelActivator Activator { get; }
    public string UrlPathSegment { get; } = "new-local-vault-form";
    public IScreen HostScreen { get; }
    public ValidationContext ValidationContext { get; } = new();

    private void AttemptToProcessForm()
    {
        TriggerFormValidations();

        if (ValidationContext.IsValid)
        {
            _localVaultBuilder.SetVaultName(LocalVaultName);
            NavigateToEncryptionAtRestFormView.Execute().Subscribe();
        }
    }

    private void TriggerFormValidations()
    {
        ValidationErrorMessagesVisible = true;
    }

    private void RegisterFormValidationRules()
    {
        RegisterVaultNameCannotBeBlankValidation();
        RegisterVaultNameMaxLengthValidation();
        RegisterVaultNameMustBeUniqueValidation();
    }

    private void RegisterVaultNameCannotBeBlankValidation()
    {
        this.ValidationRule(
            viewModel => viewModel.LocalVaultName,
            localVaultName => !string.IsNullOrWhiteSpace(localVaultName),
            "Vault name cannot be blank");
    }

    private void RegisterVaultNameMaxLengthValidation()
    {
        this.ValidationRule(
            viewModel => viewModel.LocalVaultName,
            localVaultName => localVaultName.Length <= 256,
            "Vault name cannot exceed 256 characters");
    }

    private void RegisterVaultNameMustBeUniqueValidation()
    {
        Console.WriteLine("Vault names:");
        foreach (var name in _existingLocalVaultNames) Console.WriteLine($"{name}");

        this.ValidationRule(
            viewModel => viewModel.LocalVaultName,
            localVaultName => !_existingLocalVaultNames.Contains(localVaultName),
            "Vault name must be unique");
    }
}