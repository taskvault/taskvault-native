﻿// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Reactive;
using System.Reactive.Disposables;
using ReactiveUI;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services.RoutableViewModelFactory;

namespace TaskVaultNative.Client.ViewModels;

public class MainWindowViewModel : ViewModelBase, IScreen, IActivatableViewModel
{
    private readonly ObservableAsPropertyHelper<IRoutableViewModel> _currentViewModel;

    private readonly IRoutableViewModelFactory _routableViewModelFactory;

    public MainWindowViewModel(IRoutableViewModelFactory routableViewModelFactory)
    {
        Activator = new ViewModelActivator();
        _routableViewModelFactory = routableViewModelFactory;

        _currentViewModel = Router.CurrentViewModel.ToProperty(this, x => x.CurrentViewModel);

        ResetAppForUiTesting = ReactiveCommand.Create(() =>
        {
            Router.NavigateAndReset.Execute(CreateInitialViewModel());
        });

        this.WhenActivated(disposables =>
        {
            Router.Navigate.Execute(CreateInitialViewModel());
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public IRoutableViewModel? CurrentViewModel => _currentViewModel.Value;

    public ReactiveCommand<Unit, Unit> ResetAppForUiTesting { get; }
    public ViewModelActivator Activator { get; }

    public RoutingState Router { get; } = new();

    public string? CurrentViewUrl()
    {
        return CurrentViewModel?.UrlPathSegment;
    }

    private IRoutableViewModel CreateInitialViewModel()
    {
#if DEBUG
        if (ApplicationIsRunningViaIntegrationTest())
            return _routableViewModelFactory.CreateLocalVaultSelectViewModel(this);
        // DEBUG : modify this value to change which View is initially loaded
        var vault = new LocalVaultModel { Name = "Hardcoded Test Vault", LocalVaultId = 1000 };
        //return _routableViewModelFactory.CreateProjectGroupsViewModel(this);
        return _routableViewModelFactory.CreateLocalVaultSelectViewModel(this);
        //
#else
        // todo: add logic to determine initial view based on whether vault was active when app state was captured
        return _routableViewModelFactory.CreateLocalVaultSelectViewModel(this);
#endif
        return null;
    }

    private bool ApplicationIsRunningViaIntegrationTest()
    {
        var isTestVariable = Environment.GetEnvironmentVariable("Test");
        if (isTestVariable == null)
            return false;
        return isTestVariable.ToLower() == "true";
    }
}