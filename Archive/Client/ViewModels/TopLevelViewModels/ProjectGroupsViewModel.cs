// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using ReactiveUI;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services;
using TaskVaultNative.Client.Services.RoutableViewModelFactory;

namespace TaskVaultNative.Client.ViewModels;

public class ProjectGroupsViewModel : ReactiveObject, IRoutableViewModel, IActivatableViewModel
{
    private LocalVaultModel _activeLocalVault;
    private readonly IApplicationDataStorageService _applicationDataStorage;

    private readonly IEmbeddedViewModelFactory _embeddedViewModelFactory;

    private IEnumerable<ProjectGroupModel> _loadedProjectGroups = new List<ProjectGroupModel>();
    private readonly IRoutableViewModelFactory _routableViewModelFactory;

    public ProjectGroupsViewModel(IScreen screen, IEmbeddedViewModelFactory embeddedViewModelFactory,
        IRoutableViewModelFactory routableViewModelFactory, IApplicationDataStorageService applicationDataStorage,
        LocalVaultModel activeLocalVault = null)
    {
        Activator = new ViewModelActivator();
        HostScreen = screen;

        _embeddedViewModelFactory = embeddedViewModelFactory;
        _routableViewModelFactory = routableViewModelFactory;
        _applicationDataStorage = applicationDataStorage;
        ActiveLocalVault = activeLocalVault ?? new LocalVaultModel();

        DetailsScreen = _embeddedViewModelFactory.CreateEmbeddedDetailsScreenViewModel();
        HeaderMenu = _embeddedViewModelFactory.CreateVaultHeaderMenuViewModel(HostScreen, activeLocalVault);
        ProjectGroupsPaginatedList =
            _embeddedViewModelFactory.CreatePaginatedContentListBoxViewModel<ProjectGroupModel>(HostScreen);

        this.WhenAnyValue(vm => vm.ProjectGroupsPaginatedList)
            .WhereNotNull()
            .Do(paginatedList => paginatedList.NoContentMessage = NoProjectGroupsMessage)
            .Do(paginatedList => paginatedList.NoContentMessageControlName = NoProjectGroupsMessageControlName)
            .Do(paginatedList => paginatedList.Name = ProjectGroupsListBoxControlName)
            .Do(_ => ReloadProjectGroups())
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(ProjectGroupDetailsViewModel))
            .Cast<ProjectGroupDetailsViewModel>()
            .Do(projectGroupDetailsViewModel =>
                projectGroupDetailsViewModel.OpenEditProjectGroupForm = OpenEditProjectGroupForm)
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(ProjectGroupDetailsViewModel))
            .Cast<ProjectGroupDetailsViewModel>()
            .Do(projectGroupDetailsViewModel =>
                projectGroupDetailsViewModel.DeleteProjectGroup = DeleteSelectedProjectGroup)
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(EditProjectGroupFormViewModel))
            .Cast<EditProjectGroupFormViewModel>()
            .Do(editProjectGroupFormViewModel => editProjectGroupFormViewModel.TargetProjectGroup =
                ProjectGroupsPaginatedList.SelectedItem)
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(NewProjectGroupFormViewModel))
            .Cast<NewProjectGroupFormViewModel>()
            .Select(newProjectGroupFormViewModel => newProjectGroupFormViewModel.SubmitForm)
            .Select(command => command.IsExecuting)
            .Switch()
            .Do(_ => ReloadProjectGroups())
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen.CurrentViewModel)
            .WhereNotNull()
            .Where(currentViewModel => currentViewModel.GetType() == typeof(EditProjectGroupFormViewModel))
            .Cast<EditProjectGroupFormViewModel>()
            .Select(editProjectGroupFormViewModel => editProjectGroupFormViewModel.SubmitForm)
            .Select(command => command.IsExecuting)
            .Switch()
            .Do(_ => ReloadProjectGroups())
            .Subscribe();

        this.WhenAnyValue(vm => vm.DetailsScreen)
            .WhereNotNull()
            .Do(detailsScreen => detailsScreen.NoContentMessage = DetailsScreenNoContentMessage)
            .Subscribe();

        OpenProjectGroupDetails = ReactiveCommand.CreateFromObservable(() =>
            DetailsScreen.Router.NavigateAndReset.Execute(
                _embeddedViewModelFactory.CreateProjectGroupDetailsViewModel(DetailsScreen))
        );

        // TODO: figure out how to use InvokeCommand operator
        this.WhenAnyValue(vm => vm.ProjectGroupsPaginatedList.SelectedItem)
            .WhereNotNull()
            .Select(projectGroup => projectGroup.Name)
            .Do(projectGroupName =>
            {
                OpenProjectGroupDetails.Execute().Subscribe();
                var detailsViewModel = (ProjectGroupDetailsViewModel)DetailsScreen.CurrentViewModel;
                detailsViewModel.Header = projectGroupName;
            })
            .Subscribe();

        NavigateBack = ReactiveCommand.CreateFromObservable(() => HostScreen.Router.NavigateBack.Execute());
        NavigateBack.IsExecuting
            //.Do(_ => _localVaultSession.ProjectGroup = null)
            .Subscribe();

        OpenNewProjectGroupForm = ReactiveCommand.CreateFromObservable(
            () => DetailsScreen.Router.NavigateAndReset
                .Execute(_embeddedViewModelFactory.CreateNewProjectGroupFormViewModel(DetailsScreen,
                    ActiveLocalVault)));

        OpenEditProjectGroupForm = ReactiveCommand.CreateFromObservable(() =>
            DetailsScreen.Router.NavigateAndReset.Execute(
                _embeddedViewModelFactory.CreateEditProjectGroupFormViewModel(DetailsScreen)));

        /*
         NavigateToProjectsForProjectGroup = ReactiveCommand.CreateFromObservable(() => 
            HostScreen.Router.Navigate.Execute(_routableViewModelFactory.CreateProjectsViewModel(HostScreen)));
         */
        NavigateToProjectsForProjectGroup = ReactiveCommand.CreateFromObservable(PrepareProjectsViewModel);

        OpenProjectGroup = ReactiveCommand.Create(RespondToOpenProjectGroup);

        var canDeleteSelectedProjectGroup = this.WhenAnyValue(vm => vm.ProjectGroupsPaginatedList.ItemIsSelected);
        DeleteSelectedProjectGroup =
            ReactiveCommand.Create(RespondToDeleteSelectedProjectGroup, canDeleteSelectedProjectGroup);

        DeleteSelectedProjectGroup.IsExecuting
            .Do(_ => DetailsScreen.Router.NavigationStack.Clear())
            .Subscribe();

        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public EmbeddedDetailsScreenViewModel DetailsScreen { get; }
    public VaultHeaderMenuViewModel HeaderMenu { get; }

    public ReactiveCommand<Unit, IRoutableViewModel> NavigateBack { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> OpenNewProjectGroupForm { get; }

    public ReactiveCommand<Unit, IRoutableViewModel> OpenEditProjectGroupForm { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> OpenProjectGroupDetails { get; }

    public ReactiveCommand<Unit, Unit> OpenProjectGroup { get; }
    public ReactiveCommand<Unit, IRoutableViewModel> NavigateToProjectsForProjectGroup { get; }
    public ReactiveCommand<Unit, Unit> DeleteSelectedProjectGroup { get; }

    private IEnumerable<ProjectGroupModel> LoadedProjectGroups
    {
        get => _loadedProjectGroups;
        set => this.RaiseAndSetIfChanged(ref _loadedProjectGroups, value);
    }

    public LocalVaultModel ActiveLocalVault
    {
        get => _activeLocalVault;
        private set => this.RaiseAndSetIfChanged(ref _activeLocalVault, value);
    }

    public int ProjectGroupsPerPage { get; } = 10;

    public string ProjectGroupsHeader { get; } = "Project Groups";
    public string DetailsScreenNoContentMessage { get; } = "Select Project Group to view details";

    public string NoProjectGroupsMessage { get; } =
        "No project groups in this vault. Create a project group to get started";

    public string NoProjectGroupsMessageControlName { get; } = "NoProjectGroupsMessage";

    public string ProjectGroupsListBoxControlName { get; } = "ProjectGroupsListBox";

    public PaginatedContentListBoxViewModel<ProjectGroupModel> ProjectGroupsPaginatedList { get; }
    public ViewModelActivator Activator { get; }
    public string UrlPathSegment { get; } = "project-groups-url";
    public IScreen HostScreen { get; }

    private void ReloadProjectGroups()
    {
        LoadedProjectGroups = _applicationDataStorage.LoadProjectGroupsForVault(ActiveLocalVault);
        ProjectGroupsPaginatedList.FullContent = LoadedProjectGroups.Select(items => items);
    }

    private void RespondToOpenProjectGroup()
    {
        NavigateToProjectsForProjectGroup.Execute().Subscribe();
    }

    private void RespondToDeleteSelectedProjectGroup()
    {
        _applicationDataStorage.DeleteContent(ProjectGroupsPaginatedList.SelectedItem);
        ReloadProjectGroups();
    }

    private IObservable<IRoutableViewModel> PrepareProjectsViewModel()
    {
        var projectsViewModel = _routableViewModelFactory.CreateProjectsViewModel(HostScreen,
            ProjectGroupsPaginatedList.SelectedItem);

        return HostScreen.Router.Navigate.Execute(projectsViewModel);
    }
}