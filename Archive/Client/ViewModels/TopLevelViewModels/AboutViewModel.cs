// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Reactive;
using System.Reactive.Disposables;
using ReactiveUI;
using Splat;
using TaskVaultNative.Client.Services;

namespace TaskVaultNative.Client.ViewModels;

public class AboutViewModel : ReactiveObject, IRoutableViewModel, IActivatableViewModel
{
    public AboutViewModel(IScreen screen)
    {
        Activator = new ViewModelActivator();
        HostScreen = screen;

        NavigateBack = ReactiveCommand.Create(() => { HostScreen.Router.NavigateBack.Execute(); });

        this.WhenActivated(disposables =>
        {
            
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public ViewModelActivator Activator { get; }
    public string UrlPathSegment { get; } = "about";
    public IScreen HostScreen { get; }

    public ReactiveCommand<Unit, Unit> NavigateBack { get; }
    public string AppLogoSource { get; } = Path.ChangeExtension(Path.Join("/Assets", "taskvault-logo"), ".png");
    //public string AppLogoSource { get; } = Path.ChangeExtension(Path.Join(Directory.GetCurrentDirectory(),"Assets", "taskvault-logo"), ".png");
    //public string AppLogoSource { get; } = Path.Join("/Assets", "taskvault-logo", ".png");
    //public string AppLogoSource { get; } = "/Assets/taskvault-logo.png";
    public string AppTitle { get; } = "TaskVault";
    
    public string DeveloperCredit { get; } = "Developed by Tyler Hasty: ";
    public string DeveloperWebsiteUrl { get; } = "https://tylerhasty.com/";

    public string BrandingGraphicsCredit { get; } = "Branding Graphics by Erik Hasty";
    
    public string SourceCodeReferenceLabel { get; } = "Source Code Available Here: ";
    public string SourceCodeReferenceUrl { get; } = "https://gitlab.com/taskvault/taskvault-native";
    
    public string ProjectSupportPlatformReferenceLabel { get; } = "Support the project: ";
    public string ProjectSupportPlatformReferenceUrl { get; } = "https://liberapay.com/TaskVault/";
    public string Copyright { get; } = "Copyright 2022 Tyler Hasty";
    public string Version { get; } = "v0.1.0";

    public string LicenseNotice { get; } =
        "TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public " +
        "License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version." +
        "\nTaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of " +
        "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details." +
        "\nYou should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.";
}