// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using ReactiveUI;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services;

namespace TaskVaultNative.Client.ViewModels;

public class EditTaskFormViewModel : NewTaskFormViewModel
{
    private readonly string _headerPrefix = "Editing Task - ";

    private TaskModel _targetTask;

    public EditTaskFormViewModel(IScreen screen, IApplicationDataStorageService applicationDataStorage)
        : base(screen, applicationDataStorage)
    {
        Header = "Edit Task Group";
        SubmitForm = ReactiveCommand.Create(RespondToSubmitEditForm);

        this.WhenAnyValue(vm => vm.TargetTask)
            .WhereNotNull()
            .Select(task => task.Name)
            .Do(name => Header = $"{_headerPrefix}{name}")
            .Subscribe();

        this.WhenAnyValue(vm => vm.TargetTask)
            .WhereNotNull()
            .Select(task => task.Name)
            .Do(name => TaskName = name)
            .Subscribe();

        this.WhenAnyValue(vm => vm.TargetTask)
            .WhereNotNull()
            .Select(task => task.Description)
            .WhereNotNull()
            .Do(description => TaskDescription = description)
            .Subscribe();

        this.WhenAnyValue(vm => vm.TargetTask)
            .WhereNotNull()
            .Select(task => task.Contexts)
            .Do(contexts => TaskContexts = contexts)
            .Subscribe();

        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public string UrlPathSegment { get; } = "edit-task-form";

    public TaskModel TargetTask
    {
        get => _targetTask;
        set => this.RaiseAndSetIfChanged(ref _targetTask, value);
    }

    public void RespondToSubmitEditForm()
    {
        TriggerFormValidations();

        if (ValidationContext.IsValid)
        {
            TargetTask.Name = TaskName;
            TargetTask.Description = TaskDescription;
            TargetTask.Contexts = TaskContexts;

            _applicationDataStorage.SaveContent(TargetTask);

            // TODO: navigate to details ViewModel for the newly created Project Group
            // for now, closing form instead
            CloseForm.Execute().Subscribe();
        }
    }
}