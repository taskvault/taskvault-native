// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text.RegularExpressions;
using ReactiveUI;
using ReactiveUI.Validation.Abstractions;
using ReactiveUI.Validation.Contexts;
using ReactiveUI.Validation.Extensions;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services;

namespace TaskVaultNative.Client.ViewModels;

public class NewProjectGroupFormViewModel : ReactiveObject, IRoutableViewModel, IActivatableViewModel,
    IValidatableViewModel
{
    private LocalVaultModel _activeLocalVault;

    protected IApplicationDataStorageService _applicationDataStorage;

    private readonly ObservableAsPropertyHelper<string> _errorMessages;

    // public ReactiveCommand<Unit, IRoutableViewModel> NavigateToProjectGroupDetailsViewModel { get; }

    private string _header = "New Project Group";

    private string _projectGroupName = string.Empty;

    private bool _validationErrorMessagesVisible;

    public NewProjectGroupFormViewModel(IScreen screen, IApplicationDataStorageService applicationDataStorage,
        LocalVaultModel activeLocalVault = null)
    {
        Activator = new ViewModelActivator();
        HostScreen = screen;

        _applicationDataStorage = applicationDataStorage;
        ActiveLocalVault = activeLocalVault ?? new LocalVaultModel();

        RegisterFormValidationRules();

        SubmitForm = ReactiveCommand.Create(RespondToSubmitForm);
        CloseForm = ReactiveCommand.Create(() => HostScreen.Router.NavigationStack.Clear());

        _errorMessages = this.WhenAnyValue(vm => vm.ValidationContext.Text,
                vm => vm.ValidationErrorMessagesVisible,
                (messages, flag) => messages)
            .Where(_ => ValidationErrorMessagesVisible)
            .Select(validationText => validationText.ToSingleLine("\n"))
            .ToProperty(this, vm => vm.ErrorMessages);


        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public string ErrorMessages => _errorMessages.Value;

    public string ProjectGroupName
    {
        get => _projectGroupName;
        set => this.RaiseAndSetIfChanged(ref _projectGroupName, value);
    }

    public bool ValidationErrorMessagesVisible
    {
        get => _validationErrorMessagesVisible;
        set => this.RaiseAndSetIfChanged(ref _validationErrorMessagesVisible, value);
    }

    public LocalVaultModel ActiveLocalVault
    {
        get => _activeLocalVault;
        private set => this.RaiseAndSetIfChanged(ref _activeLocalVault, value);
    }

    public ReactiveCommand<Unit, Unit> SubmitForm { get; protected set; }
    public ReactiveCommand<Unit, Unit> CloseForm { get; protected set; }

    public string Header
    {
        get => _header;
        protected set => this.RaiseAndSetIfChanged(ref _header, value);
    }

    public ViewModelActivator Activator { get; }
    public string UrlPathSegment { get; } = "new-project-group-form";
    public IScreen HostScreen { get; }

    public ValidationContext ValidationContext { get; } = new();

    private void RespondToSubmitForm()
    {
        TriggerFormValidations();

        if (ValidationContext.IsValid)
        {
            var projectGroup = new ProjectGroupModel { Name = ProjectGroupName, LocalVault = ActiveLocalVault };
            _applicationDataStorage.SaveContent(projectGroup);
            // TODO: navigate to details ViewModel for the newly created Project Group
            // for now, closing form instead
            CloseForm.Execute().Subscribe();
        }
    }

    protected void TriggerFormValidations()
    {
        ValidationErrorMessagesVisible = true;
    }

    private void RegisterFormValidationRules()
    {
        RegisterProjectGroupNameCannotBeBlankValidation();
        RegisterProjectGroupNameMaxLengthValidation();
        RegisterProjectGroupNameCharactersValidation();
    }

    private void RegisterProjectGroupNameCannotBeBlankValidation()
    {
        this.ValidationRule(
            viewModel => viewModel.ProjectGroupName,
            projectGroupName => !string.IsNullOrWhiteSpace(projectGroupName),
            "Project Group name cannot be blank");
    }

    private void RegisterProjectGroupNameMaxLengthValidation()
    {
        this.ValidationRule(
            viewModel => viewModel.ProjectGroupName,
            projectGroupName => projectGroupName.Length <= 256,
            "Project Group name cannot exceed 256 characters");
    }

    private void RegisterProjectGroupNameCharactersValidation()
    {
        var validCharactersExpression = new Regex(@"^[a-zA-Z0-9\s-_.]*$");

        this.ValidationRule(
            viewModel => viewModel.ProjectGroupName,
            projectGroupName => validCharactersExpression.IsMatch(projectGroupName),
            "Project Group name may include the following characters:\n A-Z a-z 0-9 . - _ WHITESPACE");
    }
}