// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Reactive;
using System.Reactive.Disposables;
using ReactiveUI;

namespace TaskVaultNative.Client.ViewModels;

public class ProjectGroupDetailsViewModel : ReactiveObject, IRoutableViewModel, IActivatableViewModel
{
    private ReactiveCommand<Unit, Unit> _deleteProjectGroup;

    private string _header = "Project Group Details";

    private ReactiveCommand<Unit, IRoutableViewModel> _openEditProjectGroupForm;

    public ProjectGroupDetailsViewModel(IScreen screen)
    {
        Activator = new ViewModelActivator();
        HostScreen = screen;

        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public ReactiveCommand<Unit, IRoutableViewModel> OpenEditProjectGroupForm
    {
        get => _openEditProjectGroupForm;
        set => this.RaiseAndSetIfChanged(ref _openEditProjectGroupForm, value);
    }

    public ReactiveCommand<Unit, Unit> DeleteProjectGroup
    {
        get => _deleteProjectGroup;
        set => this.RaiseAndSetIfChanged(ref _deleteProjectGroup, value);
    }

    public string Header
    {
        get => _header;
        set => this.RaiseAndSetIfChanged(ref _header, value);
    }

    public ViewModelActivator Activator { get; }
    public string UrlPathSegment { get; } = "project-group-details";
    public IScreen HostScreen { get; }
}