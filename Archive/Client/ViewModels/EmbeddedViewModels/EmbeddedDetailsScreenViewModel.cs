// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Reactive.Disposables;
using ReactiveUI;
using TaskVaultNative.Client.Services;

namespace TaskVaultNative.Client.ViewModels;

public class EmbeddedDetailsScreenViewModel : ReactiveObject, IActivatableViewModel, IScreen
{
    private readonly ObservableAsPropertyHelper<IRoutableViewModel> _currentViewModel;

    private IEmbeddedViewModelFactory _embeddedViewModelFactory;

    private string _noContentMessage = "No content to display";

    public EmbeddedDetailsScreenViewModel(IEmbeddedViewModelFactory embeddedViewModelFactory)
    {
        Activator = new ViewModelActivator();
        _embeddedViewModelFactory = embeddedViewModelFactory;

        _currentViewModel = Router.CurrentViewModel.ToProperty(this, x => x.CurrentViewModel);

        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public string NoContentMessage
    {
        get => _noContentMessage;
        set => this.RaiseAndSetIfChanged(ref _noContentMessage, value);
    }

    public IRoutableViewModel? CurrentViewModel => _currentViewModel.Value;
    public ViewModelActivator Activator { get; }

    public RoutingState Router { get; } = new();
}