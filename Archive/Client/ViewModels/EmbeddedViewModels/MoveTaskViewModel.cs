// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using ReactiveUI;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services;

namespace TaskVaultNative.Client.ViewModels;

public class MoveTaskViewModel : ReactiveObject, IRoutableViewModel, IActivatableViewModel
{
    private LocalVaultModel _activeLocalVault;
    private readonly IApplicationDataStorageService _applicationDataStorage;

    private readonly IEmbeddedViewModelFactory _embeddedViewModelFactory;

    private IEnumerable<ProjectModel> _loadedProjects = new List<ProjectModel>();

    private TaskModel _targetTask;

    public MoveTaskViewModel(IScreen screen, IEmbeddedViewModelFactory embeddedViewModelFactory,
        IApplicationDataStorageService applicationDataStorage,
        LocalVaultModel activeLocalVault = null)
    {
        Activator = new ViewModelActivator();
        HostScreen = screen;

        _embeddedViewModelFactory = embeddedViewModelFactory;
        _applicationDataStorage = applicationDataStorage;
        ActiveLocalVault = activeLocalVault ?? new LocalVaultModel();

        ProjectsPaginatedList =
            _embeddedViewModelFactory.CreatePaginatedContentListBoxViewModel<ProjectModel>(HostScreen);

        this.WhenAnyValue(vm => vm.ProjectsPaginatedList)
            .WhereNotNull()
            .Do(paginatedList => paginatedList.NoContentMessage = NoProjectsMessage)
            .Subscribe();

        this.WhenAnyValue(vm => vm.TargetTask)
            .WhereNotNull()
            .Do(_ => ReloadProjects())
            .Subscribe();

        CloseForm = ReactiveCommand.Create(() => HostScreen.Router.NavigationStack.Clear());

        var canMoveTaskToProject = this.WhenAnyValue(vm => vm.ProjectsPaginatedList.SelectedItem)
            .Select(item => item != null);
        MoveTaskToProject = ReactiveCommand.Create(RespondToMoveTaskToProject, canMoveTaskToProject);

        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public ReactiveCommand<Unit, Unit> MoveTaskToProject { get; }
    public ReactiveCommand<Unit, Unit> CloseForm { get; }

    public TaskModel TargetTask
    {
        get => _targetTask;
        set => this.RaiseAndSetIfChanged(ref _targetTask, value);
    }

    public LocalVaultModel ActiveLocalVault
    {
        get => _activeLocalVault;
        private set => this.RaiseAndSetIfChanged(ref _activeLocalVault, value);
    }

    private IEnumerable<ProjectModel> LoadedProjects
    {
        get => _loadedProjects;
        set => this.RaiseAndSetIfChanged(ref _loadedProjects, value);
    }

    public PaginatedContentListBoxViewModel<ProjectModel> ProjectsPaginatedList { get; }

    public string Header { get; } = "Move Task to Project";

    public string NoProjectsMessage { get; } = "No projects available which task can be moved to";
    public ViewModelActivator Activator { get; }
    public string UrlPathSegment { get; } = "move-task";
    public IScreen HostScreen { get; }

    private void RespondToMoveTaskToProject()
    {
        TargetTask.ProjectId = ProjectsPaginatedList.SelectedItem.ProjectId;
        TargetTask.Project = ProjectsPaginatedList.SelectedItem;

        _applicationDataStorage.SaveContent(TargetTask);

        CloseForm.Execute().Subscribe();
    }

    private void ReloadProjects()
    {
        LoadedProjects = _applicationDataStorage.LoadProjectsInLocalVault(ActiveLocalVault);

        if (TargetTask.ProjectId != 0)
            LoadedProjects = LoadedProjects.Where(project => project.ProjectId != TargetTask.ProjectId);

        ProjectsPaginatedList.FullContent = LoadedProjects.ToList();
    }
}