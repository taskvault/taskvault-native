// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Reactive;
using System.Reactive.Disposables;
using ReactiveUI;

namespace TaskVaultNative.Client.ViewModels;

public class TaskDetailsViewModel : ReactiveObject, IRoutableViewModel, IActivatableViewModel
{
    private bool _canMarkComplete;

    private bool _canMoveTask;

    private ReactiveCommand<Unit, Unit> _deleteTask;

    private string _header = "Task Details";

    private ReactiveCommand<Unit, Unit> _markAsCompleted;

    private ReactiveCommand<Unit, IRoutableViewModel> _openEditTaskForm;

    private ReactiveCommand<Unit, IRoutableViewModel> _openMoveTask;

    private string _taskContexts = "No contexts";

    private string _taskDescription = "";

    public TaskDetailsViewModel(IScreen screen)
    {
        Activator = new ViewModelActivator();
        HostScreen = screen;

        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public ReactiveCommand<Unit, IRoutableViewModel> OpenEditTaskForm
    {
        get => _openEditTaskForm;
        set => this.RaiseAndSetIfChanged(ref _openEditTaskForm, value);
    }

    public ReactiveCommand<Unit, Unit> DeleteTask
    {
        get => _deleteTask;
        set => this.RaiseAndSetIfChanged(ref _deleteTask, value);
    }

    public ReactiveCommand<Unit, Unit> MarkAsCompleted
    {
        get => _markAsCompleted;
        set => this.RaiseAndSetIfChanged(ref _markAsCompleted, value);
    }

    public ReactiveCommand<Unit, IRoutableViewModel> OpenMoveTask
    {
        get => _openMoveTask;
        set => this.RaiseAndSetIfChanged(ref _openMoveTask, value);
    }

    public string Header
    {
        get => _header;
        set => this.RaiseAndSetIfChanged(ref _header, value);
    }

    public string TaskDescription
    {
        get => _taskDescription;
        set => this.RaiseAndSetIfChanged(ref _taskDescription, value);
    }

    public string TaskContexts
    {
        get => _taskContexts;
        set => this.RaiseAndSetIfChanged(ref _taskContexts, value);
    }

    public bool CanMarkComplete
    {
        get => _canMarkComplete;
        set => this.RaiseAndSetIfChanged(ref _canMarkComplete, value);
    }

    public bool CanMoveTask
    {
        get => _canMoveTask;
        set => this.RaiseAndSetIfChanged(ref _canMoveTask, value);
    }

    public ViewModelActivator Activator { get; }
    public string UrlPathSegment { get; } = "task-details";
    public IScreen HostScreen { get; }
}