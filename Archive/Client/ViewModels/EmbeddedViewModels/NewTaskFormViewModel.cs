// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text.RegularExpressions;
using ReactiveUI;
using ReactiveUI.Validation.Abstractions;
using ReactiveUI.Validation.Contexts;
using ReactiveUI.Validation.Extensions;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services;

namespace TaskVaultNative.Client.ViewModels;

// TODO: just copied this from NewProjectViewModel, need to update as appropriate
public class NewTaskFormViewModel : ReactiveObject, IRoutableViewModel, IActivatableViewModel, IValidatableViewModel
{
    private LocalVaultModel _activeLocalVault;

    private ProjectModel _activeProject;

    protected IApplicationDataStorageService _applicationDataStorage;

    private readonly ObservableAsPropertyHelper<string> _errorMessages;

    private string _header = "New Task";

    private IEnumerable<TaskContextModel> _loadedTaskContexts = new List<TaskContextModel>();

    private readonly ObservableAsPropertyHelper<bool> _noTaskContextsExist;

    private ObservableCollection<TaskContextModel> _selectedTaskContexts = new();

    private ObservableCollection<TaskContextModel> _taskContexts = new();

    private string _taskDescription = string.Empty;

    private string _taskName = string.Empty;

    private bool _validationErrorMessagesVisible;

    public NewTaskFormViewModel(IScreen screen, IApplicationDataStorageService applicationDataStorage,
        LocalVaultModel activeLocalVault = null,
        ProjectModel activeProject = null)
    {
        Activator = new ViewModelActivator();
        HostScreen = screen;

        _applicationDataStorage = applicationDataStorage;
        if (activeProject != null)
            ActiveLocalVault = _applicationDataStorage.LoadLocalVaultForProject(activeProject);
        else if (activeLocalVault != null)
            ActiveLocalVault = activeLocalVault;
        else
            ActiveLocalVault = new LocalVaultModel();
        ActiveProject = activeProject ?? new ProjectModel();

        RegisterFormValidationRules();

        SubmitForm = ReactiveCommand.Create(RespondToSubmitForm);
        CloseForm = ReactiveCommand.Create(() => HostScreen.Router.NavigationStack.Clear());

        _errorMessages = this.WhenAnyValue(vm => vm.ValidationContext.Text,
                vm => vm.ValidationErrorMessagesVisible,
                (messages, flag) => messages)
            .Where(_ => ValidationErrorMessagesVisible)
            .Select(validationText => validationText.ToSingleLine("\n"))
            .ToProperty(this, vm => vm.ErrorMessages);

        _noTaskContextsExist = this.WhenAnyValue(vm => vm.LoadedTaskContexts)
            .Select(taskContexts => taskContexts)
            .Select(taskContexts => taskContexts.Count() == 0)
            .ToProperty(this, vm => vm.NoTaskContextsExist);

        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public string ErrorMessages => _errorMessages.Value;

    public string TaskName
    {
        get => _taskName;
        set => this.RaiseAndSetIfChanged(ref _taskName, value);
    }

    public string TaskDescription
    {
        get => _taskDescription;
        set => this.RaiseAndSetIfChanged(ref _taskDescription, value);
    }

    public ObservableCollection<TaskContextModel> TaskContexts
    {
        get => _taskContexts;
        set => this.RaiseAndSetIfChanged(ref _taskContexts, value);
    }

    public bool ValidationErrorMessagesVisible
    {
        get => _validationErrorMessagesVisible;
        set => this.RaiseAndSetIfChanged(ref _validationErrorMessagesVisible, value);
    }

    public string Header
    {
        get => _header;
        protected set => this.RaiseAndSetIfChanged(ref _header, value);
    }

    public ObservableCollection<TaskContextModel> SelectedTaskContexts
    {
        get => _selectedTaskContexts;
        set => this.RaiseAndSetIfChanged(ref _selectedTaskContexts, value);
    }

    public IEnumerable<TaskContextModel> LoadedTaskContexts
    {
        get => _loadedTaskContexts;
        set => this.RaiseAndSetIfChanged(ref _loadedTaskContexts, value);
    }

    public LocalVaultModel ActiveLocalVault
    {
        get => _activeLocalVault;
        private set => this.RaiseAndSetIfChanged(ref _activeLocalVault, value);
    }

    public ProjectModel ActiveProject
    {
        get => _activeProject;
        set => this.RaiseAndSetIfChanged(ref _activeProject, value);
    }

    public bool NoTaskContextsExist => _noTaskContextsExist.Value;

    public ReactiveCommand<Unit, Unit> SubmitForm { get; protected set; }
    public ReactiveCommand<Unit, Unit> CloseForm { get; protected set; }

    public string NoTaskContextsMessage { get; } =
        "No task contexts exist for this vault. To assign a task context to this task, first create a task context";

    public ViewModelActivator Activator { get; }
    public string UrlPathSegment { get; } = "new-project-form";
    public IScreen HostScreen { get; }

    public ValidationContext ValidationContext { get; } = new();

    private void RespondToSubmitForm()
    {
        TriggerFormValidations();

        if (ValidationContext.IsValid)
        {
            var task = new TaskModel
            {
                Name = TaskName, Description = TaskDescription, Contexts = TaskContexts,
                LocalVaultId = ActiveLocalVault.LocalVaultId
            };
            if (ActiveProject.ProjectId != 0) task.Project = ActiveProject;
            _applicationDataStorage.SaveContent(task);
            // TODO: navigate to details ViewModel for the newly created Project
            // for now, closing form instead
            CloseForm.Execute().Subscribe();
        }
    }

    protected void TriggerFormValidations()
    {
        ValidationErrorMessagesVisible = true;
    }

    private void RegisterFormValidationRules()
    {
        RegisterNameCannotBeBlankValidation();
        RegisterNameMaxLengthValidation();
        RegisterNameCharactersValidation();
        RegisterDescriptionMaxLengthValidation();
    }

    private void RegisterNameCannotBeBlankValidation()
    {
        this.ValidationRule(
            viewModel => viewModel.TaskName,
            name => !string.IsNullOrWhiteSpace(name),
            "Task name cannot be blank");
    }

    private void RegisterNameMaxLengthValidation()
    {
        this.ValidationRule(
            viewModel => viewModel.TaskName,
            name => name.Length <= 256,
            "Task name cannot exceed 256 characters");
    }

    private void RegisterNameCharactersValidation()
    {
        var validCharactersExpression = new Regex(@"^[a-zA-Z0-9\s-_.]*$");

        this.ValidationRule(
            viewModel => viewModel.TaskName,
            name => validCharactersExpression.IsMatch(name),
            "Task name may include the following characters:\n A-Z a-z 0-9 . - _ WHITESPACE");
    }

    private void RegisterDescriptionMaxLengthValidation()
    {
        this.ValidationRule(viewModel => viewModel.TaskDescription,
            description => description.Length <= 5000,
            "Task description may not exceed 5000 characters");
    }
}