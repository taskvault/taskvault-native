// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text.RegularExpressions;
using ReactiveUI;
using ReactiveUI.Validation.Abstractions;
using ReactiveUI.Validation.Contexts;
using ReactiveUI.Validation.Extensions;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.Services;

namespace TaskVaultNative.Client.ViewModels;

public class NewProjectFormViewModel : ReactiveObject, IRoutableViewModel, IActivatableViewModel, IValidatableViewModel
{
    private ProjectGroupModel _activeProjectGroup;

    protected IApplicationDataStorageService _applicationDataStorage;

    private readonly ObservableAsPropertyHelper<string> _errorMessages;

    private string _header = "New Project";

    private string _projectName = string.Empty;

    private bool _validationErrorMessagesVisible;

    public NewProjectFormViewModel(IScreen screen, IApplicationDataStorageService applicationDataStorage,
        ProjectGroupModel activeProjectGroup = null)
    {
        Activator = new ViewModelActivator();
        HostScreen = screen;

        _applicationDataStorage = applicationDataStorage;
        ActiveProjectGroup = activeProjectGroup ?? new ProjectGroupModel();

        RegisterFormValidationRules();

        SubmitForm = ReactiveCommand.Create(RespondToSubmitForm);
        CloseForm = ReactiveCommand.Create(() => HostScreen.Router.NavigationStack.Clear());

        _errorMessages = this.WhenAnyValue(vm => vm.ValidationContext.Text,
                vm => vm.ValidationErrorMessagesVisible,
                (messages, flag) => messages)
            .Where(_ => ValidationErrorMessagesVisible)
            .Select(validationText => validationText.ToSingleLine("\n"))
            .ToProperty(this, vm => vm.ErrorMessages);


        this.WhenActivated(disposables =>
        {
            /* handle activation */
            Disposable
                .Create(() =>
                {
                    /* handle deactivation */
                })
                .DisposeWith(disposables);
        });
    }

    public string ErrorMessages => _errorMessages.Value;

    public string ProjectName
    {
        get => _projectName;
        set => this.RaiseAndSetIfChanged(ref _projectName, value);
    }

    public ProjectGroupModel ActiveProjectGroup
    {
        get => _activeProjectGroup;
        set => this.RaiseAndSetIfChanged(ref _activeProjectGroup, value);
    }

    public bool ValidationErrorMessagesVisible
    {
        get => _validationErrorMessagesVisible;
        set => this.RaiseAndSetIfChanged(ref _validationErrorMessagesVisible, value);
    }

    public ReactiveCommand<Unit, Unit> SubmitForm { get; protected set; }
    public ReactiveCommand<Unit, Unit> CloseForm { get; protected set; }

    public string Header
    {
        get => _header;
        protected set => this.RaiseAndSetIfChanged(ref _header, value);
    }

    public ViewModelActivator Activator { get; }
    public string UrlPathSegment { get; } = "new-project-form";
    public IScreen HostScreen { get; }

    public ValidationContext ValidationContext { get; } = new();

    private void RespondToSubmitForm()
    {
        TriggerFormValidations();

        if (ValidationContext.IsValid)
        {
            var project = new ProjectModel { Name = ProjectName, ProjectGroup = ActiveProjectGroup };
            _applicationDataStorage.SaveContent(project);
            // TODO: navigate to details ViewModel for the newly created Project
            // for now, closing form instead
            CloseForm.Execute().Subscribe();
        }
    }

    protected void TriggerFormValidations()
    {
        ValidationErrorMessagesVisible = true;
    }

    private void RegisterFormValidationRules()
    {
        RegisterProjectNameCannotBeBlankValidation();
        RegisterProjectNameMaxLengthValidation();
        RegisterProjectNameCharactersValidation();
    }

    private void RegisterProjectNameCannotBeBlankValidation()
    {
        this.ValidationRule(
            viewModel => viewModel.ProjectName,
            projectName => !string.IsNullOrWhiteSpace(projectName),
            "Project name cannot be blank");
    }

    private void RegisterProjectNameMaxLengthValidation()
    {
        this.ValidationRule(
            viewModel => viewModel.ProjectName,
            projectName => projectName.Length <= 256,
            "Project name cannot exceed 256 characters");
    }

    private void RegisterProjectNameCharactersValidation()
    {
        var validCharactersExpression = new Regex(@"^[a-zA-Z0-9\s-_.]*$");

        this.ValidationRule(
            viewModel => viewModel.ProjectName,
            projectName => validCharactersExpression.IsMatch(projectName),
            "Project name may include the following characters:\n A-Z a-z 0-9 . - _ WHITESPACE");
    }
}