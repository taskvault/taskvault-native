<!-- 
Copyright 2022 Tyler Hasty

This file is part of TaskVault (also herein referred to as taskvault-native).

TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.
-->
<UserControl xmlns="https://github.com/avaloniaui"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
             mc:Ignorable="d" d:DesignWidth="800" d:DesignHeight="450"
             x:Class="TaskVaultNative.Client.Views.LocalVaultSelectView"
             xmlns:vm="using:TaskVaultNative.Client.ViewModels"
             xmlns:embeddedViews="clr-namespace:TaskVaultNative.Client.Views.EmbeddedViews"
             xmlns:m="clr-namespace:TaskVaultNative.Client.Models"
             xmlns:services="clr-namespace:TaskVaultNative.Client.Services"
             x:DataType="vm:LocalVaultSelectViewModel"
             x:CompileBindings="True">
    
    <UserControl.Resources>
        <services:BitmapAssetValueConverter x:Key="VariableImage"/>
    </UserControl.Resources>
    
    <Grid RowDefinitions="2*, 10*, 3*, 1*" MinWidth="400">
        
        <Image Grid.Row="0" 
               Name="TaskVaultLogo"
               Source="{Binding TaskVaultLogoSource, Converter={StaticResource VariableImage}}"
               Width="256"
               Height="144"
               HorizontalAlignment="Center"
               VerticalAlignment="Center"/>

        <embeddedViews:PaginatedContentListBoxView Grid.Row="1" Width="400"
                                                   DataContext="{Binding LocalVaultsPaginatedList}">
            <embeddedViews:PaginatedContentListBoxView.DataTemplates>
                <DataTemplate DataType="{x:Type m:LocalVaultModel}">
                    <TextBlock
                        Name="LocalVaultName"
                        Text="{ReflectionBinding Name}"
                        TextAlignment="Center"
                        VerticalAlignment="Stretch" />
                </DataTemplate>
            </embeddedViews:PaginatedContentListBoxView.DataTemplates>
        </embeddedViews:PaginatedContentListBoxView>
        
        <Image Grid.Row="2" 
               Name="GplV3Logo"
               Source="{Binding GplV3LogoSource, Converter={StaticResource VariableImage}}"
               Width="136"
               Height="68"
               Margin="30,0,0,0"
               HorizontalAlignment="Left"
               VerticalAlignment="Bottom"/>
        
        <StackPanel Grid.Row="2" HorizontalAlignment="Center" Width="140" Margin="0,32,0,0">
            <Button Name="OpenVaultButton"
                    Content="Open Vault"
                    Command="{Binding OpenSelectedVault}"
                    HorizontalAlignment="Stretch"
                    HorizontalContentAlignment="Center"
                    Margin="0,8,0,16" />
            <Button Name="NewLocalVaultButton"
                    Content="New Local Vault"
                    Command="{Binding NavigateToNewLocalVaultFormView }"
                    HorizontalAlignment="Stretch"
                    HorizontalContentAlignment="Center" />
        </StackPanel>
        
        <StackPanel Grid.Row="3" Margin="0,8,0,8">
            <TextBlock Grid.Row="3" Name="Copyright" 
                       Text="{Binding Copyright}"
                       FontSize="13"
                       FontWeight="Thin"
                       TextAlignment="Center" 
                       HorizontalAlignment="Left"
                       VerticalAlignment="Center"
                       Margin="32, 0, 0, 8" />
        
            <TextBlock Grid.Row="3" Name="Version"
                       Text="{Binding Version}"
                       FontSize="14"
                       FontWeight="Thin"
                       TextAlignment="Center" 
                       HorizontalAlignment="Left"
                       VerticalAlignment="Center"
                       Margin="32, 0, 0, 0"/>
        </StackPanel>
        
        <StackPanel Grid.Row="3" Orientation="Horizontal" HorizontalAlignment="Center" Margin="0,10,0,6">
            <TextBlock Name="RoadmapAndFeedbackLabel"
                       Text="{Binding RoadmapAndFeedbackLabel}"
                       FontSize="10"
                       TextWrapping="Wrap"
                       HorizontalAlignment="Center"
                       VerticalAlignment="Center"
                       TextAlignment="Center"/>
            <TextBox Name="RoadmapAndFeedbackUrl" 
                     Text="{Binding RoadmapAndFeedbackUrl, Mode=OneWay}"
                     FontSize="10"
                     VerticalAlignment="Center"
                     HorizontalContentAlignment="Center"
                     TextAlignment="Center"
                     Classes="Url"/>
        </StackPanel>
        
        <Button Grid.Row="3" Name="AboutButton"
                Content="About"
                Command="{Binding NavigateToAbout}"
                HorizontalAlignment="Right"
                HorizontalContentAlignment="Center"
                VerticalAlignment="Center"
                VerticalContentAlignment="Center"
                FontSize="12"
                Width="72"
                Height="32"
                Margin="0,0,32,22"
                Background="#00000000"/>
        
    </Grid>

</UserControl>