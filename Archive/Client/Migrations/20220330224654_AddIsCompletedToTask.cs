﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskVaultNative.Client.Migrations
{
    public partial class AddIsCompletedToTask : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsCompleted",
                table: "TaskModels",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsCompleted",
                table: "TaskModels");
        }
    }
}
