﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using TaskVaultNative.Client.Services;

#nullable disable

namespace TaskVaultNative.Client.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20220329230613_FixCascadeDeleteForProject")]
    partial class FixCascadeDeleteForProject
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "6.0.3");

            modelBuilder.Entity("TaskVaultNative.Client.Models.LocalVaultModel", b =>
                {
                    b.Property<int>("LocalVaultId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<bool?>("EncryptionAtRestEnabled")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("SyncId")
                        .HasColumnType("TEXT");

                    b.HasKey("LocalVaultId");

                    b.HasIndex("SyncId")
                        .IsUnique();

                    b.ToTable("LocalVaultModels");
                });

            modelBuilder.Entity("TaskVaultNative.Client.Models.ProjectGroupModel", b =>
                {
                    b.Property<int>("ProjectGroupId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("LocalVaultId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<Guid?>("SyncId")
                        .HasColumnType("TEXT");

                    b.HasKey("ProjectGroupId");

                    b.HasIndex("LocalVaultId");

                    b.HasIndex("SyncId")
                        .IsUnique();

                    b.ToTable("ProjectGroupModels");
                });

            modelBuilder.Entity("TaskVaultNative.Client.Models.ProjectModel", b =>
                {
                    b.Property<int>("ProjectId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<int>("ProjectGroupId")
                        .HasColumnType("INTEGER");

                    b.HasKey("ProjectId");

                    b.HasIndex("ProjectGroupId");

                    b.ToTable("ProjectModels");
                });

            modelBuilder.Entity("TaskVaultNative.Client.Models.TaskContextModel", b =>
                {
                    b.Property<int>("TaskContextId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("LocalVaultId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<int?>("TaskModelTaskId")
                        .HasColumnType("INTEGER");

                    b.HasKey("TaskContextId");

                    b.HasIndex("LocalVaultId");

                    b.HasIndex("TaskModelTaskId");

                    b.ToTable("TaskContextModels");
                });

            modelBuilder.Entity("TaskVaultNative.Client.Models.TaskModel", b =>
                {
                    b.Property<int>("TaskId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Description")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<int?>("ProjectId")
                        .IsRequired()
                        .HasColumnType("INTEGER");

                    b.HasKey("TaskId");

                    b.HasIndex("ProjectId");

                    b.ToTable("TaskModels");
                });

            modelBuilder.Entity("TaskVaultNative.Client.Models.ProjectGroupModel", b =>
                {
                    b.HasOne("TaskVaultNative.Client.Models.LocalVaultModel", "LocalVault")
                        .WithMany("ProjectGroups")
                        .HasForeignKey("LocalVaultId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("LocalVault");
                });

            modelBuilder.Entity("TaskVaultNative.Client.Models.ProjectModel", b =>
                {
                    b.HasOne("TaskVaultNative.Client.Models.ProjectGroupModel", "ProjectGroup")
                        .WithMany("Projects")
                        .HasForeignKey("ProjectGroupId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("ProjectGroup");
                });

            modelBuilder.Entity("TaskVaultNative.Client.Models.TaskContextModel", b =>
                {
                    b.HasOne("TaskVaultNative.Client.Models.LocalVaultModel", "LocalVault")
                        .WithMany()
                        .HasForeignKey("LocalVaultId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("TaskVaultNative.Client.Models.TaskModel", null)
                        .WithMany("Contexts")
                        .HasForeignKey("TaskModelTaskId");

                    b.Navigation("LocalVault");
                });

            modelBuilder.Entity("TaskVaultNative.Client.Models.TaskModel", b =>
                {
                    b.HasOne("TaskVaultNative.Client.Models.ProjectModel", "Project")
                        .WithMany("Tasks")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.ClientCascade)
                        .IsRequired();

                    b.Navigation("Project");
                });

            modelBuilder.Entity("TaskVaultNative.Client.Models.LocalVaultModel", b =>
                {
                    b.Navigation("ProjectGroups");
                });

            modelBuilder.Entity("TaskVaultNative.Client.Models.ProjectGroupModel", b =>
                {
                    b.Navigation("Projects");
                });

            modelBuilder.Entity("TaskVaultNative.Client.Models.ProjectModel", b =>
                {
                    b.Navigation("Tasks");
                });

            modelBuilder.Entity("TaskVaultNative.Client.Models.TaskModel", b =>
                {
                    b.Navigation("Contexts");
                });
#pragma warning restore 612, 618
        }
    }
}
