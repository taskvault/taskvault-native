﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskVaultNative.Client.Migrations
{
    public partial class AddTaskAndTaskContextAndUpdateProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TaskModels",
                columns: table => new
                {
                    TaskId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    Description = table.Column<string>(type: "TEXT", nullable: false),
                    ProjectId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskModels", x => x.TaskId);
                    table.ForeignKey(
                        name: "FK_TaskModels_ProjectModels_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "ProjectModels",
                        principalColumn: "ProjectId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TaskContextModels",
                columns: table => new
                {
                    TaskContextId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    LocalVaultId = table.Column<int>(type: "INTEGER", nullable: false),
                    TaskModelTaskId = table.Column<int>(type: "INTEGER", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskContextModels", x => x.TaskContextId);
                    table.ForeignKey(
                        name: "FK_TaskContextModels_LocalVaultModels_LocalVaultId",
                        column: x => x.LocalVaultId,
                        principalTable: "LocalVaultModels",
                        principalColumn: "LocalVaultId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TaskContextModels_TaskModels_TaskModelTaskId",
                        column: x => x.TaskModelTaskId,
                        principalTable: "TaskModels",
                        principalColumn: "TaskId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_TaskContextModels_LocalVaultId",
                table: "TaskContextModels",
                column: "LocalVaultId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskContextModels_TaskModelTaskId",
                table: "TaskContextModels",
                column: "TaskModelTaskId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskModels_ProjectId",
                table: "TaskModels",
                column: "ProjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TaskContextModels");

            migrationBuilder.DropTable(
                name: "TaskModels");
        }
    }
}
