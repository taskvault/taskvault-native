﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskVaultNative.Client.Migrations
{
    public partial class AddProjectAndUpdateProjectGroup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProjectModels",
                columns: table => new
                {
                    ProjectId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: false),
                    ProjectGroupId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectModels", x => x.ProjectId);
                    table.ForeignKey(
                        name: "FK_ProjectModels_ProjectGroupModels_ProjectGroupId",
                        column: x => x.ProjectGroupId,
                        principalTable: "ProjectGroupModels",
                        principalColumn: "ProjectGroupId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProjectModels_ProjectGroupId",
                table: "ProjectModels",
                column: "ProjectGroupId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProjectModels");
        }
    }
}
