﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskVaultNative.Client.Migrations
{
    public partial class TaskProjectIdCanBeNull : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskModels_ProjectModels_ProjectId",
                table: "TaskModels");

            migrationBuilder.AlterColumn<int>(
                name: "ProjectId",
                table: "TaskModels",
                type: "INTEGER",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "INTEGER");

            migrationBuilder.AddForeignKey(
                name: "FK_TaskModels_ProjectModels_ProjectId",
                table: "TaskModels",
                column: "ProjectId",
                principalTable: "ProjectModels",
                principalColumn: "ProjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskModels_ProjectModels_ProjectId",
                table: "TaskModels");

            migrationBuilder.AlterColumn<int>(
                name: "ProjectId",
                table: "TaskModels",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "INTEGER",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskModels_ProjectModels_ProjectId",
                table: "TaskModels",
                column: "ProjectId",
                principalTable: "ProjectModels",
                principalColumn: "ProjectId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
