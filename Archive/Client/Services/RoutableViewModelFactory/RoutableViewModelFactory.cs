// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

namespace TaskVaultNative.Client.Services.RoutableViewModelFactory;

public partial class RoutableViewModelFactory : IRoutableViewModelFactory
{
    private readonly IApplicationDataStorageService _applicationDataStorageService;
    private readonly IEmbeddedViewModelFactory _embeddedViewModelFactory;
    private readonly ILocalVaultBuilderService _localVaultBuilderService;
    private readonly IBitmapAssetValueConverter _bitmapAssetValueConverter;

    public RoutableViewModelFactory(ILocalVaultBuilderService localVaultBuilderService,
        IApplicationDataStorageService applicationDataStorageService, IBitmapAssetValueConverter bitmapAssetValueConverter)
    {
        _localVaultBuilderService = localVaultBuilderService;
        _applicationDataStorageService = applicationDataStorageService;
        _embeddedViewModelFactory = new EmbeddedViewModelFactory(this,
            _applicationDataStorageService);
        _bitmapAssetValueConverter = bitmapAssetValueConverter;
    }
}