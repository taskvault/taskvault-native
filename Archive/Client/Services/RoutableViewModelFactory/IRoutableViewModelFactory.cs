// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using ReactiveUI;
using TaskVaultNative.Client.Models;
using TaskVaultNative.Client.ViewModels;

namespace TaskVaultNative.Client.Services.RoutableViewModelFactory;

public interface IRoutableViewModelFactory
{
    // TODO: why not split this factory into individual classes for each View Model? Any advantages or disadvantages?
    // TODO: given the differences between Android and Desktop life-cylces, can View Models be abstracted in any meaningful way?
    // Or, is the only motivation for registering View Models with the Service Container the fact this allows View Models to receive dependencies by injection?
    public LocalVaultSelectViewModel CreateLocalVaultSelectViewModel(IScreen screen);
    public NewLocalVaultFormViewModel CreateNewLocalVaultFormViewModel(IScreen screen);
    public EncryptionAtRestFormViewModel CreateEncryptionAtRestFormViewModel(IScreen screen);
    public ConnectToRemoteFormViewModel CreateConnectToRemoteFormViewModel(IScreen screen);
    public MainMenuViewModel CreateMainMenuViewModel(IScreen screen, LocalVaultModel activeLocalVault = null);
    public ProjectGroupsViewModel CreateProjectGroupsViewModel(IScreen screen, LocalVaultModel activeLocalVault = null);
    public ProjectsViewModel CreateProjectsViewModel(IScreen screen, ProjectGroupModel activeProjectGroup = null);
    public TasksViewModel CreateTasksViewModel(IScreen screen, ProjectModel activeProject = null);
    public InboxViewModel CreateInboxViewModel(IScreen screen, LocalVaultModel activeLocalVault = null);
    public AboutViewModel CreateAboutViewModel(IScreen screen);
}