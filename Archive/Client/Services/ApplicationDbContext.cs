// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.IO;
using Castle.Core.Internal;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using TaskVaultNative.Client.Models;

namespace TaskVaultNative.Client.Services;

public class ApplicationDbContext : DbContext
{
    private readonly IConfiguration _configuration;
    private string _connectionString;
    private readonly string _databaseFilename;

    public ApplicationDbContext(IConfiguration configuration)
    {
        _configuration = configuration;
        _databaseFilename = _configuration.GetConnectionString("ApplicationDatabase");
    }

    public DbSet<LocalVaultModel> LocalVaultModels { get; set; }
    public DbSet<ProjectGroupModel> ProjectGroupModels { get; set; }
    public DbSet<ProjectModel> ProjectModels { get; set; }
    public DbSet<TaskModel> TaskModels { get; set; }
    public DbSet<TaskContextModel> TaskContextModels { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var TaskVaultDataDirectoryPath = Path.Join(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "TaskVault");

        if (Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData).IsNullOrEmpty() 
            || Environment.GetEnvironmentVariable("Test") == "true")
        {
            TaskVaultDataDirectoryPath = Path.Join(Directory.GetCurrentDirectory(), "data");
        }
        if (!Directory.Exists(TaskVaultDataDirectoryPath)) Directory.CreateDirectory(TaskVaultDataDirectoryPath);
        var dbFilepath = Path.Join(TaskVaultDataDirectoryPath, _databaseFilename);

        _connectionString = $"Data Source={dbFilepath}";
        optionsBuilder.UseSqlite(_connectionString);
        optionsBuilder.UseChangeTrackingProxies();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        ConfigureCascadingDeleteForProjectTasks(modelBuilder);
    }

    private void ConfigureCascadingDeleteForProjectTasks(ModelBuilder modelBuilder)
    {
        modelBuilder
            .Entity<ProjectModel>()
            .HasMany(model => model.Tasks)
            .WithOne(model => model.Project)
            .IsRequired(false)
            .OnDelete(DeleteBehavior.ClientCascade);
    }
}