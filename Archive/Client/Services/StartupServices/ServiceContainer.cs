// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Reflection;
using Avalonia.ReactiveUI;
using Avalonia.Threading;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ReactiveUI;
using Splat;
using Splat.Microsoft.Extensions.DependencyInjection;
using TaskVaultNative.Client.Services.RoutableViewModelFactory;
using TaskVaultNative.Client.ViewModels;

namespace TaskVaultNative.Client.Services;

public class ServiceContainer : IServiceProvider
{
    private readonly IConfiguration _appConfiguration;
    private IServiceProvider _serviceProvider;
    private IServiceCollection? _services;

    public ServiceContainer(IConfiguration appConfiguration)
    {
        _appConfiguration = appConfiguration;
        ConfigureServices();
    }

    public object? GetService(Type serviceType)
    {
        return _serviceProvider.GetService(serviceType);
    }

    private void ConfigureServices()
    {
        ConfigureFrameworkToUseMicrosoftDI();
        RegisterServices();
        BuildServiceProviderFromRegisteredServices();
        ReregisterFrameworkServices();
    }

    private void ConfigureFrameworkToUseMicrosoftDI()
    {
        _services.UseMicrosoftDependencyResolver();
    }

    private void RegisterServices()
    {
        _services = new ServiceCollection();

        RegisterAppConfigurationAsService();
        RegisterEntrypointAsService();
        RegisterCustomServices();
        RegisterViewsAsServices();
    }

    private void RegisterAppConfigurationAsService()
    {
        _services.AddSingleton(_appConfiguration);
    }

    private void RegisterEntrypointAsService()
    {
        _services.AddSingleton<MainWindowViewModel>();
    }

    private void RegisterCustomServices()
    {
        _services.AddSingleton<ILocalVaultBuilderService, LocalVaultBuilderService>();
        _services.AddSingleton<IRoutableViewModelFactory, RoutableViewModelFactory.RoutableViewModelFactory>();
        _services.AddDbContextFactory<ApplicationDbContext>();
        _services.AddDbContext<ApplicationDbContext>();
        _services.AddSingleton<IApplicationDataStorageService, ApplicationSqLiteService>();
        _services.AddSingleton<IBitmapAssetValueConverter, BitmapAssetValueConverter>();
        // Note: EmbeddedViewModelFactory is instantiated directly in RoutableViewModelFactory
    }

    private void RegisterViewsAsServices()
    {
        Locator.CurrentMutable.RegisterViewsForViewModels(Assembly.GetExecutingAssembly());
    }

    private void BuildServiceProviderFromRegisteredServices()
    {
        _serviceProvider = _services.BuildServiceProvider();
    }

    private void ReregisterFrameworkServices()
    {
        Locator.CurrentMutable.InitializeSplat();
        Locator.CurrentMutable.InitializeReactiveUI();

        RxApp.MainThreadScheduler = AvaloniaScheduler.Instance;
        Locator.CurrentMutable.RegisterConstant(new AvaloniaActivationForViewFetcher(),
            typeof(IActivationForViewFetcher));
        Locator.CurrentMutable.RegisterConstant(new AutoDataTemplateBindingHook(), typeof(IPropertyBindingHook));
    }
}