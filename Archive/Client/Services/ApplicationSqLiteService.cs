// Copyright 2022 Tyler Hasty
// 
// This file is part of TaskVault (also herein referred to as taskvault-native).
// 
// TaskVault is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
// 
// TaskVault is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License along with TaskVault. If not, see <https://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using DynamicData.Kernel;
using Microsoft.EntityFrameworkCore;
using TaskVaultNative.Client.Models;

namespace TaskVaultNative.Client.Services;

public class ApplicationSqLiteService : IApplicationDataStorageService
{
    private readonly IDbContextFactory<ApplicationDbContext> _dbContextFactory;

    public ApplicationSqLiteService(IDbContextFactory<ApplicationDbContext> dbContextFactory)
    {
        _dbContextFactory = dbContextFactory;
    }

    public void SaveContent<TModel>(TModel contentModel)
    {
        using (var dbContext = _dbContextFactory.CreateDbContext())
        {
            // TODO: generate SyncId if contentModel.SyncId exists and is null
            if (dbContext.Entry(contentModel) == null)
                dbContext.Add(contentModel);
            else
                dbContext.Update(contentModel);

            dbContext.SaveChanges();
        }
    }

    public LocalVaultModel LoadVaultByName(string vaultName)
    {
        LocalVaultModel vaultModel;

        using (var dbContext = _dbContextFactory.CreateDbContext())
        {
            // TODO: this method assumes there is only one local vault with a specific name, but this is not enforced by the Save operation presently
            vaultModel = dbContext.LocalVaultModels.Single(model => model.Name == vaultName);
        }

        return vaultModel;
    }

    public IEnumerable<LocalVaultModel> LoadLocalVaults()
    {
        IEnumerable<LocalVaultModel> localVaultModels;

        using (var dbContext = _dbContextFactory.CreateDbContext())
        {
            localVaultModels = dbContext.LocalVaultModels.AsList();
        }

        return localVaultModels;
    }

    public IEnumerable<ProjectGroupModel> LoadProjectGroupsForVault(LocalVaultModel localVaultModel)
    {
        IEnumerable<ProjectGroupModel> projectGroupModels;

        using (var dbContext = _dbContextFactory.CreateDbContext())
        {
            projectGroupModels =
                dbContext.ProjectGroupModels.Where(projectGroup => projectGroup.LocalVault == localVaultModel).AsList();
        }

        return projectGroupModels;
    }

    public IEnumerable<ProjectModel> LoadProjectsInProjectGroup(ProjectGroupModel projectGroupModel)
    {
        IEnumerable<ProjectModel> projectModels;

        using (var dbContext = _dbContextFactory.CreateDbContext())
        {
            projectModels = dbContext.ProjectModels.Where(project => project.ProjectGroup == projectGroupModel)
                .AsList();
        }

        return projectModels;
    }

    public virtual IEnumerable<TaskContextModel> LoadTaskContextsForVault(LocalVaultModel localVaultModel)
    {
        IEnumerable<TaskContextModel> taskContextModels;

        using (var dbContext = _dbContextFactory.CreateDbContext())
        {
            taskContextModels = dbContext.TaskContextModels
                .Where(taskContextModel => taskContextModel.LocalVault == localVaultModel).AsList();
        }

        return taskContextModels;
    }

    public IEnumerable<TaskModel> LoadTasksInProject(ProjectModel projectModel)
    {
        IEnumerable<TaskModel> taskModels;

        using (var dbContext = _dbContextFactory.CreateDbContext())
        {
            taskModels = dbContext.TaskModels
                .Where(taskModel => taskModel.Project == projectModel).AsList();

            foreach (var task in taskModels) dbContext.Entry(task).Collection(task => task.Contexts).Load();
        }

        return taskModels;
    }

    public void DeleteContent<TModel>(TModel contentModel)
    {
        using (var dbContext = _dbContextFactory.CreateDbContext())
        {
            if (dbContext.Entry(contentModel) != null) dbContext.Remove(contentModel);

            dbContext.SaveChanges();
        }
    }

    public void DeleteProjectGroup(ProjectGroupModel projectGroupModel)
    {
        using (var dbContext = _dbContextFactory.CreateDbContext())
        {
            /* approach-A
            projectGroupModel = dbContext.ProjectGroupModels
                .Where(model => model.ProjectGroupId == projectGroupModel.ProjectGroupId)
                .Include(model => model.Projects)
                .ThenInclude(model => model.Tasks)
                .Single();
                */
            /* approach-B
            var projectModels = dbContext.ProjectModels
                .Where(model => model.ProjectGroupId == projectGroupModel.ProjectGroupId).ToList();

            foreach (var model in projectModels)
            {
                DeleteProject(model);
            }

            projectGroupModel =
                dbContext.ProjectGroupModels.Single(model => model.ProjectGroupId == projectGroupModel.ProjectGroupId);
                */

            projectGroupModel.Projects =
                new ObservableCollection<ProjectModel>(RecursivelyLoadProjectsInProjectGroup(projectGroupModel));

            dbContext.Remove(projectGroupModel);

            dbContext.SaveChanges();
        }
    }

    public void DeleteProject(ProjectModel projectModel)
    {
        using (var dbContext = _dbContextFactory.CreateDbContext())
        {
            /*
            var taskModels = dbContext.TaskModels
                .Where(model => model.ProjectId == projectModel.ProjectId).ToList();

            foreach (var model in taskModels)
            {
                DeleteTask(model);
            }
            
            projectModel = dbContext.ProjectModels.Single(model => model.ProjectId == projectModel.ProjectId);
            */

            projectModel.Tasks = new ObservableCollection<TaskModel>(RecursivelyLoadTasksInProject(projectModel));

            dbContext.Remove(projectModel);

            dbContext.SaveChanges();
        }
    }

    public void DeleteTask(TaskModel taskModel)
    {
        using (var dbContext = _dbContextFactory.CreateDbContext())
        {
            dbContext.Remove(taskModel);

            dbContext.SaveChanges();
        }
    }

    public IEnumerable<TaskModel> LoadTasksInInbox(LocalVaultModel localVaultModel)
    {
        IEnumerable<TaskModel> taskModels;

        using (var dbContext = _dbContextFactory.CreateDbContext())
        {
            taskModels = GetTasksWithNoAssociatedProject(dbContext);

            taskModels = taskModels.Where(task => task.LocalVaultId == localVaultModel.LocalVaultId);

            foreach (var task in taskModels) dbContext.Entry(task).Collection(task => task.Contexts).Load();
        }

        return taskModels;
    }

    public IEnumerable<ProjectModel> LoadProjectsInLocalVault(LocalVaultModel localVaultModel)
    {
        var projects = new List<ProjectModel>();
        var projectGroups = LoadProjectGroupsForVault(localVaultModel);
        foreach (var projectGroup in projectGroups)
        {
            var projectsInProjectGroup = LoadProjectsInProjectGroup(projectGroup);
            projects.AddRange(projectsInProjectGroup);
        }

        return projects;
    }

    public LocalVaultModel LoadLocalVaultForProjectGroup(ProjectGroupModel projectGroupModel)
    {
        LocalVaultModel localVaultModel;

        using (var dbContext = _dbContextFactory.CreateDbContext())
        {
            localVaultModel =
                dbContext.LocalVaultModels.Single(localVault =>
                    localVault.LocalVaultId == projectGroupModel.LocalVaultId);
        }

        return localVaultModel;
    }

    public LocalVaultModel LoadLocalVaultForProject(ProjectModel projectModel)
    {
        LocalVaultModel localVaultModel;

        using (var dbContext = _dbContextFactory.CreateDbContext())
        {
            localVaultModel = dbContext.ProjectModels.Where(project => project.ProjectId == projectModel.ProjectId)
                .Select(project => project.ProjectGroup)
                .Select(projectGroup => projectGroup.LocalVault).Single();
        }

        return localVaultModel;
    }

    private IEnumerable<ProjectModel> RecursivelyLoadProjectsInProjectGroup(ProjectGroupModel projectGroupModel)
    {
        var projectModels = LoadProjectsInProjectGroup(projectGroupModel);
        foreach (var projectModel in projectModels)
            projectModel.Tasks = new ObservableCollection<TaskModel>(RecursivelyLoadTasksInProject(projectModel));

        return projectModels;
    }

    private IEnumerable<TaskModel> RecursivelyLoadTasksInProject(ProjectModel projectModel)
    {
        var taskModels = LoadTasksInProject(projectModel);

        return taskModels;
    }

    private IEnumerable<TaskModel> GetTasksWithNoAssociatedProject(ApplicationDbContext dbContext)
    {
        IEnumerable<TaskModel> taskModels = dbContext.TaskModels
            .Where(taskModel => taskModel.ProjectId == null).AsList();

        return taskModels;
    }
}