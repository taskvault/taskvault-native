![TaskVault Logo](./readme-assets/taskvault-logo.png)

# TaskVault

[taskvault.io](https://taskvault.io) 

TaskVault is a [Libre/ Free and Open Source](https://www.gnu.org/philosophy/free-sw.html) 
privacy-friendly [Getting-Things-Done](https://gettingthingsdone.com/what-is-gtd/) 
compatible personal project management and productivity application for native
platforms.

[**Download TaskVault**](https://gitlab.com/taskvault/taskvault-native/-/releases)

[Report a bug](https://taskvault.canny.io/bug-reports)

[View roadmap](https://taskvault.canny.io/)

[Request features, vote on feature requests and provide feedback](https://taskvault.canny.io/feature-requests)

[Join Matrix room](https://matrix.to/#/#taskvault-general:matrix.org)

[Become a Sponsor](https://liberapay.com/TaskVault/)

[**Subscribe to development email newsletter**](https://taskvault.hosted.phplist.com/lists/?p=subscribe&id=1)

**Have general feedback or questions?** Please send an email to _team@taskvault.io_

## Features Overview

**Platforms to be supported initially:** Android, Linux, Windows

- designed to enable enhanced productivity without sacrificing data privacy
- manage life's complexity in confidence, without worrying whether unintended parties may gain access
  to sensitive information.

**Note:** TaskVault is in early development and as such, features and UI/UX are 
expected to change and improve

![TaskVault Screen Shot 01](readme-assets/Screenshots/taskvault-screenshot-01.png)

- Create a local (offline) task-vault to get started

![TaskVault Screen Shot 04](readme-assets/Screenshots/taskvault-screenshot-04.png)

- Use Inbox to capture active tasks

![TaskVault Screen Shot 02](readme-assets/Screenshots/taskvault-screenshot-02.png)

- Organize tasks into actionable goals via the project-hierarchy:
Project Categories -> Projects -> Tasks

![TaskVault Screen Shot 03](readme-assets/Screenshots/taskvault-screenshot-03.png)

![TaskVault Screen Shot 05](readme-assets/Screenshots/taskvault-screenshot-05.png)

![TaskVault Screen Shot 06](readme-assets/Screenshots/taskvault-screenshot-06.png)

## What is Getting Things Done?

Getting Things Done is a personal productivity system developed by David Allen and detailed in a
published and well regarded book of the same name.

**Note**: TaskVault and its contributors are NOT affiliated with David Allen Company, 
Getting Things Done, or any related business entities or organizations. 

Instead, TaskVault is an independent tool developed to be utilized
as part of a personal Getting-Things-Done-system implementation. Of course, there is no requirement to practice
GTD when using TaskVault; users are free to employ TaskVault in whichever ways work best for their
use case and circumstances.

Please see the following resources for more information on Getting Things Done:

[Getting Things Done official website](https://gettingthingsdone.com/)

[Getting Things Done (ebook)](https://www.ebooks.com/en-us/book/1710332/getting-things-done/david-allen/)

## Please Support the Project

<noscript><a href="https://liberapay.com/TaskVault/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>

In addition to being freedom-software, the base application will remain free-of-cost.

If you would like to sponsor this project, please consider [scheduling a recurring
donation](https://liberapay.com/TaskVault/)

Your support is highly appreciated!

## About the Developer

Developed by [Tyler Hasty](https://tylerhasty.com)

Branding Graphics by Erik Hasty

## Development Info

TaskVault native desktop prototype was originally implemented in C# 
with [AvaloniaUI](http://avaloniaui.net/) crossplatform framework.
For better platform support and improved development productivity, 
the project is being ported to Kotlin Multiplatform, where
the Android and Desktop clients will be developed concurrently with the help
of [Android JetPack Compose](https://developer.android.com/courses/jetpack-compose/course)
and [Kotlin Multiplatform Compose](https://www.jetbrains.com/lp/compose-mpp/) respectively.


### Build Instructions

TODO

(for now, reference .gitlab-ci.yml and docker-compose.yml)

## Feedback and Questions

TODO

## Licensing and Copying Info

![GPLv3Logo](./readme-assets/gplv3-with-text-136x68.png)

Please see LICENSE-NOTICE.txt and COPYING.txt for TaskVault licensing information.